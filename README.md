# Battlecraft
A tool for creating 3D battle maps for RPG games

## Description
I implemented this project as a part of "CS553 Advanced Computer Graphics" for my Master's. 

By using Battlecraft, one can sculpt and paint a procedurally generated terrain however he likes. Then he can place foliage and other 3D models to enrich the scenery!

## Documentation
Complete [report](https://docs.google.com/document/d/11oiU3gTpvR_ymHkcwbNauEmfSakdoc7J-IYTzsx5awQ/edit?usp=sharing) regarding the implementation of the game:

## Video Sample
Click [here](https://drive.google.com/file/d/1e7_eUg3G1pYtjawa8_Xu3Kpj7-WIarNZ/view?usp=sharing) to see a video of the application running (Greek Dub)

## Short Presentation
Click [here](https://docs.google.com/presentation/d/1i3ftBfGRDVYxb_6Fzz9MCEGL9ZxfVDJh1qvRj1ztD5U/edit?usp=sharing) to see a short presentation I prepared for the project.
