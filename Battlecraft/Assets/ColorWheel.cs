using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ColorWheel : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    public Color color;
    public UnityEvent onColorChange = new UnityEvent();

    private bool isDragging;

    [SerializeField]    
    private RawImage wheel;

    [SerializeField]
    private RectTransform pointerTransform;

    private void UpdateColor()
    {
        Vector3 position = Input.mousePosition - wheel.GetComponent<RectTransform>().position;
        pointerTransform.localPosition = position;

        Vector3Int pixelPosition = Vector3Int.FloorToInt(new Vector3(position.x - wheel.mainTexture.width/2, position.y - wheel.mainTexture.height/2));
        color = (wheel.mainTexture as Texture2D).GetPixel(pixelPosition.x, pixelPosition.y);
        onColorChange.Invoke();
    }

    private void Update()
    {
        if (Input.GetMouseButton(0) && !IsMouseOver())
            gameObject.SetActive(false);
    }

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    private bool IsMouseOver()
    {
        PointerEventData eventData = new PointerEventData(EventSystem.current);
        eventData.position = Input.mousePosition;
        List<RaycastResult> raycastResults = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventData, raycastResults);

        bool isMouseOver = false;
        foreach (RaycastResult result in raycastResults)
        {
            if (result.gameObject == this.gameObject)
            {
                isMouseOver = true;
            }
        }
        return isMouseOver;
    }
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // Drag Events
    void IBeginDragHandler.OnBeginDrag(PointerEventData eventData)
    {
        isDragging = true;
        UpdateColor();
    }

    public void     OnDrag(PointerEventData eventData)
    {
        UpdateColor();
    }

    public void     OnEndDrag(PointerEventData eventData)
    {
        isDragging = false;
        UpdateColor();
    }
}
