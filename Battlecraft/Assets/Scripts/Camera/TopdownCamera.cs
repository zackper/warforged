using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class TopdownCamera : MonoBehaviour
{
    public float panSensitivity = 0.02f;
    public float zoomSensitivity = 0.2f;
    public float maxZoomSize = 50;
    public float minZoomSize = 1;

    private Camera cam;
    private Vector3 previousPosition;

    private void Start()
    {
        cam = GetComponent<Camera>();
    }
    private void Update()
    {
        HandlePan();
        HandleZoom();
    }

    // Handle camera movements
    private void HandlePan()
    {
        if (Input.GetMouseButtonDown(1))
        {
            previousPosition = Input.mousePosition;
        }
        else if (Input.GetMouseButton(1))
        {
            Vector3 difference = Input.mousePosition - previousPosition;
            previousPosition = Input.mousePosition;

            transform.position -= new Vector3(difference.x, 0, difference.y) * panSensitivity;
        }
    }
    private void HandleZoom()
    {
        if(
            (Input.mouseScrollDelta.y < 0 && cam.orthographicSize < maxZoomSize) ||
            (Input.mouseScrollDelta.y > 0 && cam.orthographicSize > minZoomSize)
        )
        cam.orthographicSize -= Input.mouseScrollDelta.y * zoomSensitivity;
    }
}
