using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CameraType
{
    FLY,
    TOPDOWN
}

public class CameraManager : MonoBehaviour
{
    [SerializeField]
    private Camera flyCamera;
    [SerializeField]
    private Camera topdownCamera;

    private Dictionary<CameraType, Camera> dispatcher = new Dictionary<CameraType, Camera>();

    private void Start()
    {
        dispatcher.Add(CameraType.FLY, flyCamera);
        dispatcher.Add(CameraType.TOPDOWN, topdownCamera);
    }

    public void ChangeCamera(CameraType type)
    {
        foreach(KeyValuePair<CameraType, Camera> pair in dispatcher)
        {
            if (pair.Key == type)
                pair.Value.gameObject.SetActive(true);
            else
                pair.Value.gameObject.SetActive(false);
        }
    }

    // Singleton Properties
    public static CameraManager Instance { get; private set; }
    private void Awake()
    {
        // If there is an instance, and it's not me, delete myself.

        if (Instance != null && Instance != this)
        {
            Destroy(this);
        }
        else
        {
            Instance = this;
        }
    }
}
