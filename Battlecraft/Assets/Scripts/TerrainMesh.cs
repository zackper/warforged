using UnityEngine;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine.Events;


[RequireComponent(typeof(MeshFilter))]
public class TerrainMesh : MonoBehaviour
{
    public int subLevel = 3;
    public int sizeX = 15;
    public int sizeZ = 15;
    
    public UnityEvent onTerrainUpdate = new UnityEvent();
    public UnityEvent updateTerrain = new UnityEvent();

    Mesh mesh;

    Vector3[] vertices;
    int[] triangles;
    Vector2[] uvs;

    // Start is called before the first frame update
    void Start()
    {
        mesh = new Mesh();
        mesh.indexFormat = UnityEngine.Rendering.IndexFormat.UInt32;
        GetComponent<MeshFilter>().mesh = mesh;

        CreateShape();

        // Subscribe to event for terrain needs update
        updateTerrain.AddListener(OnTerrainNeedsUpdate);
    }

    private void OnTerrainNeedsUpdate()
    {
        CreateShape();
        UpdateMesh();
    }

    // Initially create mesh. This needs to be called for every major mesh change
    private void CreateShape()
    {
        // Create Vertices
        sizeX = sizeX * subLevel;
        sizeZ = sizeZ * subLevel;
        vertices = new Vector3[(sizeX + 1) * (sizeZ + 1)];
        for (int i = 0, z = 0; z <= sizeZ; z++)
        {
            for (int x = 0; x <= sizeX; x++)
            {
                float y = Mathf.PerlinNoise(x * 0.3f, z * 0.3f) * 0.2f;
                vertices[i++] = new Vector3((float)x/subLevel, y, (float)z/subLevel);
            }
        }

        // Create Triangles
        triangles = new int[sizeX * sizeZ * 6];
        int vert = 0;
        int tris = 0;
        for (int z = 0; z < sizeZ; z++)
        {
            for (int x = 0; x < sizeX; x++)
            {
                triangles[tris + 0] = vert + 0;
                triangles[tris + 1] = vert + sizeX + 1;
                triangles[tris + 2] = vert + 1;
                triangles[tris + 3] = vert + 1;
                triangles[tris + 4] = vert + sizeX + 1;
                triangles[tris + 5] = vert + sizeX + 2;

                vert++;
                tris += 6;
            }
            vert++;
        }

        // Create UVs;
        uvs = new Vector2[vertices.Length];
        for (int i = 0, z = 0; z <= sizeZ; z++)
        {
            for (int x = 0; x <= sizeX; x++)
            {
                uvs[i++] = new Vector2((float)x / sizeX, (float)z / sizeZ);
            }
        }

        // Add texture to material
        this.GetComponent<Renderer>().material.mainTexture = new Texture2D(4096, 4096);

        UpdateMesh();
        UpdateCollider();

        // Register self to grid
        RegisterToGrid();
    }

    // If values of vertex attributes have been updated, call this function
    public void UpdateMesh()
    {
        mesh.Clear();
        mesh.vertices = vertices;
        mesh.triangles = triangles;
        mesh.uv = uvs;

        mesh.RecalculateNormals();
        mesh.RecalculateBounds();

        onTerrainUpdate.Invoke();
    }

    // Update Mesh Collider
    public void UpdateCollider()
    {
        Destroy(GetComponent<MeshCollider>());
        gameObject.AddComponent<MeshCollider>();
    }

    // Register vertices to grid manager.
    public void RegisterToGrid()
    {
        GridManager.Instance.InitGrid(sizeX, sizeZ);
        for(int z = 0; z < sizeZ; z++)
        {
            for(int x = 0; x < sizeX; x++)
            {
                int index = z * (sizeX + 1) + x;
                Vector3 pos = vertices[index];
                GridManager.Instance.grid.Add(pos.x, pos.z, index);
            }
        }
        GridManager.Instance.grid.AssertPreconditions();
        //StartCoroutine(GridManager.Instance.grid.ShowAllVertices(this));
    }

    // Return ref to prevent the copying of the array
    public ref Vector3[] GetVerticesRef()
    {
        return ref vertices;
    }

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // Util functions
    // Wrapper function to make raycasting simpler
    public bool RaycastTerrain(Vector3 screenPoint, out RaycastHit hitInfo, bool ignoreUI = false, int mask = -1)
    {
        Ray ray = Camera.main.ScreenPointToRay(screenPoint);
        if (Physics.Raycast(ray, out hitInfo, 550, mask) && (ignoreUI || !EventSystem.current.IsPointerOverGameObject()))
        {
            if (hitInfo.transform.gameObject == this.gameObject)
            {
                return true;
            }
        }

        return false;
    }

    public int      FindClosestVertexIndex(Vector3 position)
    {
        int x = Mathf.RoundToInt(position.x * subLevel);
        int z = Mathf.RoundToInt(position.z * subLevel);

        return z * (sizeX + 1) + x;
    }
    public Vector3  FindClosestVertex(Vector3 position)
    {
        return vertices[FindClosestVertexIndex(position)];
    }

    public Vector2  GetTrueSize()
    {
        return new Vector2(sizeX / subLevel, sizeZ / subLevel);
    }

    public float    GetVertexDistance()
    {
        return Vector2.Distance(new Vector2(vertices[0].x, vertices[0].z), new Vector2(vertices[1].x, vertices[1].z));
    }

    public List<int> GetVertexNeighbors(int index)
    {
        Vector3 pos = vertices[index];

        List<int> neighbors = new List<int>();
        float vDistance = GetVertexDistance();
        for(int m = -1; m <= 1; m++)
        {
            for(int n = -1; n <= 1; n++)
            {
                if (m != 0 || n != 0)
                {
                    int neighborIndex = FindClosestVertexIndex(new Vector3(pos.x + (float)m * vDistance, 0, pos.z + (float)n * vDistance));
                    neighbors.Add(neighborIndex);
                }
            }
        }

        return neighbors;
    }

    public List<Vector3> gizmoStack1 = new List<Vector3>();
    public List<Vector3> gizmoStack2 = new List<Vector3>();
    public List<Vector3> gizmoStack3 = new List<Vector3>();
    private void OnDrawGizmos()
    {
        if (mesh == null)
            return;

        foreach(Vector3 pos in gizmoStack1)
            Gizmos.DrawSphere(pos, 0.25f);
        foreach (Vector3 pos in gizmoStack2)
            Gizmos.DrawSphere(pos, 0.5f);
        foreach (Vector3 pos in gizmoStack3)
            Gizmos.DrawCube(pos, Vector3.one * 0.1f);

        for (int z = 0; z < 50; z++)
        {
            for (int x = 0; x < 50; x++)
            {
                int index = z * sizeX + x;
                Gizmos.DrawSphere(vertices[index], 0.1f);
            }
        }
    }
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
}