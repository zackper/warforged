using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class InputManager : MonoBehaviour
{
    private Dictionary<string, KeyCode> buttonMap = new Dictionary<string, KeyCode>();
    private Dictionary<string, UnityEvent> buttonEvents = new Dictionary<string, UnityEvent>();

    private void Update()
    {
        foreach(KeyValuePair<string, KeyCode> pair in buttonMap)
        {
            if (Input.GetKeyDown(pair.Value))
                buttonEvents[pair.Key].Invoke();
        }
    }

    public void AddButton(string name, KeyCode code)
    {
        if (buttonMap.ContainsKey(name))
            buttonMap.Remove(name);
        buttonMap.Add(name, code);
    }
    public void Subscribe(string buttonName, UnityAction callback)
    {
        Debug.Assert(buttonEvents.ContainsKey(buttonName), "Button does not exist \'" + buttonName + "\'");
        buttonEvents[buttonName].AddListener(callback);
    }

    // Called in singleton awake
    private void InitButtonMap()
    {
        AddButton("tab", KeyCode.Tab);
        buttonEvents.Add("tab", new UnityEvent());

        AddButton("1", KeyCode.Alpha1);
        buttonEvents.Add("1", new UnityEvent());
        AddButton("2", KeyCode.Alpha2);
        buttonEvents.Add("2", new UnityEvent());
        AddButton("3", KeyCode.Alpha3);
        buttonEvents.Add("3", new UnityEvent());
        AddButton("4", KeyCode.Alpha4);
        buttonEvents.Add("4", new UnityEvent());
        AddButton("5", KeyCode.Alpha5);
        buttonEvents.Add("5", new UnityEvent());
        AddButton("6", KeyCode.Alpha6);
        buttonEvents.Add("6", new UnityEvent());
    }


    #region Singleton Pattern
    public static InputManager Instance { get; private set; }
    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this);
            return;
        }
        Instance = this;
        InitButtonMap();
        DontDestroyOnLoad(this);
    }
    #endregion
}

