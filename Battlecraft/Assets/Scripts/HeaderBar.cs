using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HeaderBar : MonoBehaviour
{
    public Color selectedColor;
    public Color unselectedColor;

    public List<GameObject> systemTools;

    private void Start()
    {
        ChangeTool(4);

        // Set Shortcuts
        InputManager.Instance.Subscribe("1", () => ChangeTool(0));
        InputManager.Instance.Subscribe("2", () => ChangeTool(1));
        InputManager.Instance.Subscribe("3", () => ChangeTool(2));
        InputManager.Instance.Subscribe("4", () => ChangeTool(3));
        InputManager.Instance.Subscribe("5", () => ChangeTool(4));
        InputManager.Instance.Subscribe("6", () => ChangeTool(5));
    }

    public void ChangeTool(int type)
    {
        // Assume type is alligned with header buttons
        for(int i = 0; i < systemTools.Count; i++)
        {
            Image image = systemTools[i].GetComponent<Image>();
            if(i == type)
                image.color = selectedColor;
            else
                image.color = unselectedColor;
        }


        SystemToolManager.Instance.ChangeTool((SystemToolType)type);
    }
}
