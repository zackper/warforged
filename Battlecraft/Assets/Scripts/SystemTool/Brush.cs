using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Brush
{
    public Dictionary<string, float> properties = new Dictionary<string, float>();
    private LineRenderer lr;

    public Brush()
    {
        // Default property will be radius
        properties.Add("radius", 5);
    }
    ~Brush()
    {
        MonoBehaviour.Destroy(lr.gameObject);
    }

    /// <summary>
    /// Draws brush cursor of property.radius at point. Point should be at global space.
    /// </summary>
    /// <param name="point"></param>
    public void DrawCursor(Vector3 point)
    {
        Debug.Assert(properties.ContainsKey("radius"));

        int detailSteps = 32;
        lr.positionCount = detailSteps;

        for(int i = 0; i < detailSteps; i++)
        {
            float circumferenceProgress = (float)i / detailSteps;
            float currentRadian = circumferenceProgress * 2 * Mathf.PI;
            float xScaled = Mathf.Cos(currentRadian);
            float yScaled = Mathf.Sin(currentRadian);

            float x = xScaled * properties["radius"];
            float y = yScaled * properties["radius"];

            Vector3 currentPosition = new Vector3(x, 0, y) + point;
            lr.SetPosition(i, currentPosition);
        }
    }

    public void CreateCursor()
    {
        // Create Object for the line renderer of the cursor
        GameObject lrObj = new GameObject("Brush Line Renderer");
        lr = lrObj.AddComponent<LineRenderer>();
        lr.startWidth = 0.2f;
        lr.endWidth = 0.2f;
        lr.loop = true;
        lr.material = new Material(Shader.Find("Sprites/Default"));
        lr.startColor = Color.white;
        lr.endColor = Color.white;
    }
    public void DeleteCursor()
    {
        MonoBehaviour.Destroy(lr.gameObject);
    }
}
