using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SystemToolUI;

public class TerrainWeatherTool : SystemTool
{
    // List of UI elements
    private List<GameObject> uiElements = new List<GameObject>();
    // Reference to terrain
    private TerrainMesh terrain;

    // References to scene
    private Light sun;

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // Interface Functions
    public void Initialize(TerrainMesh terrain)
    {
        // Set terrain reference
        this.terrain = terrain;
        // Set scene references
        this.sun = GameObject.Find("Sun").GetComponent<Light>();
    }
    public void GenerateUI()
    {
        // SUN SETTINGS
        uiElements.Add(UIFactoryHelper.CreateCategory("Sun Settings"));
        // Color Picker
        ColorPicker sunColor = UIFactoryHelper.CreateColorPicker("sun color", new Color(1, 0.9568627f, 0.8392157f));
        sunColor.onColorChange.AddListener(delegate
        {
            sun.color = sunColor.color;
        });
        uiElements.Add(sunColor.gameObject);
        // Intensity
        Slider intensity = UIFactoryHelper.CreateSlider("Intensity", 0, 5, 1);
        intensity.onValueChanged.AddListener(delegate
        {
            sun.intensity = intensity.value;
        });
        intensity.onValueChanged.Invoke();
        uiElements.Add(intensity.gameObject);
        // Time of Day
        Slider timeOfDay = UIFactoryHelper.CreateSlider("Time of Day", 0, 24, 6);
        timeOfDay.postfix = ":00";
        timeOfDay.format = "F0";
        timeOfDay.onValueChanged.AddListener(delegate
        {
            float yEulerAngle = sun.transform.eulerAngles.y;
            float mappedValue = ExtensionMethods.Map(timeOfDay.value, 0, 24, 0, 360);
            sun.transform.localEulerAngles = new Vector3(mappedValue, 0f, 0f);
            sun.transform.eulerAngles = new Vector3(sun.transform.eulerAngles.x, yEulerAngle, 0f);
        });
        timeOfDay.onValueChanged.Invoke();
        uiElements.Add(timeOfDay.gameObject);
        // Y Direction
        Slider yDirection = UIFactoryHelper.CreateSlider("Y Direction", 0, 360, 0);
        yDirection.postfix = "�";
        yDirection.onValueChanged.AddListener(delegate
        {
            float mappedValue = ExtensionMethods.Map(yDirection.value, 0, 360, 0, 360);
            sun.transform.eulerAngles = new Vector3(sun.transform.eulerAngles.x, mappedValue, 0f);
        });
        yDirection.onValueChanged.Invoke();
        uiElements.Add(yDirection.gameObject);

        // FOG SETTINGS
        uiElements.Add(UIFactoryHelper.CreateCategory("Fog Settings"));
        // Color Picker
        ColorPicker fogColor = UIFactoryHelper.CreateColorPicker("fog color", new Color(1, 0.9568627f, 0.8392157f));
        fogColor.onColorChange.AddListener(delegate
        {
            RenderSettings.fogColor = fogColor.color;
        });
        uiElements.Add(fogColor.gameObject);
        // Density
        Slider fogDensity = UIFactoryHelper.CreateSlider("Density", 0, 0.3f, 0.01f);
        fogDensity.format = "F2";
        fogDensity.onValueChanged.AddListener(delegate
        {
            RenderSettings.fogDensity = fogDensity.value;
        });
        fogDensity.onValueChanged.Invoke();
        uiElements.Add(fogDensity.gameObject);

        //// Other settings
        //uiElements.Add(UIFactoryHelper.CreateCategory("Misc Settings"));
        //Slider cloudDensity = UIFactoryHelper.CreateSlider("Density", 0, 1, 0);
        //cloudDensity.onValueChanged.AddListener(delegate
        //{
        //});
        //cloudDensity.onValueChanged.Invoke();
        //uiElements.Add(fogDensity.gameObject);
        //Slider rainDensity = UIFactoryHelper.CreateSlider("Density", 0, 1, 0);
        //rainDensity.onValueChanged.AddListener(delegate
        //{
        //});
        //rainDensity.onValueChanged.Invoke();
        //uiElements.Add(rainDensity.gameObject);
    }
    public void Start()
    {
        
    }
    public void Stop()
    {
    }
    public void Update()
    {
        
    }
    public void PopulateToolPanel(ToolPanel toolPanel)
    {
        foreach (GameObject uiElement in uiElements)
            toolPanel.Add(uiElement);
    }
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
}


public static class ExtensionMethods
{
    public static float Map(this float value, float fromSource, float toSource, float fromTarget, float toTarget)
    {
        return (value - fromSource) / (toSource - fromSource) * (toTarget - fromTarget) + fromTarget;
    }
}