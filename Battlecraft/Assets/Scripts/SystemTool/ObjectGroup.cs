using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectGroup
{
    private List<GameObject> list = new List<GameObject>();

    private bool[,] densityMatrix;
    private float tileSize;

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    public void InitDensityMatrix(float tileSize)
    {
        Grid grid = GridManager.Instance.grid;
        int sizeX = (int)(grid.sizeX * grid.GetTileUnit());
        int sizeZ = (int)(grid.sizeZ * grid.GetTileUnit());

        this.tileSize = tileSize;
        densityMatrix = new bool[(int)Mathf.Ceil(sizeX / tileSize), (int)Mathf.Ceil(sizeZ / tileSize)];
        for (int i = 0; i < densityMatrix.GetLength(0); i++)
            for (int j = 0; j < densityMatrix.GetLength(1); j++)
                densityMatrix[i, j] = false;
    }
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    public bool CanBeAdded(Vector3 position, float density)
    {
        int totalCells = (int)(densityMatrix.GetLength(0) * densityMatrix.GetLength(1));
        int densityCells = (int)(totalCells *  density / 100);
        int spaceBetween = (int)totalCells / densityCells;

        position.y = 0;
        Vector3Int matrixPos = Vector3Int.FloorToInt(position / tileSize);
        if (
            matrixPos.x < 0 || matrixPos.x > densityMatrix.GetLength(0) ||
            matrixPos.z < 0 || matrixPos.z > densityMatrix.GetLength(1)
        )
            return false;

        // Get total bounds of object (may contain encapsulated objects)
        Queue<Vector3Int> bfsQueue = new Queue<Vector3Int>();
        Dictionary<Vector3, bool> checkedCells = new Dictionary<Vector3, bool>();
        bfsQueue.Enqueue(matrixPos);

        while(bfsQueue.Count > 0) { 
            Vector3Int cell = bfsQueue.Dequeue();
            if (checkedCells.ContainsKey(cell))
                continue;

            if (densityMatrix[cell.x, cell.z] == true)
                return false;
            else
            {
                // Check if it has exceeded distance of search
                if (Vector3Int.Distance(matrixPos, cell) > spaceBetween)
                    break;
                // Or add neighbors to list and continue expanding
                for (int m = -1; m <= 1; m++)
                    for (int n = -1; n <= 1; n++)
                        if (
                            cell.x + m >= 0 && cell.x + m < densityMatrix.GetLength(0)  &&
                            cell.z + n >= 0 && cell.z + n < densityMatrix.GetLength(1)  &&
                            Mathf.Abs(m - n) != 0
                        )
                        {
                            bfsQueue.Enqueue(cell + new Vector3Int(m, 0, n));
                        }
                checkedCells.Add(cell, true);
            }
        }

        return true;
    }
    public void Add(GameObject obj)
    {
        list.Add(obj);
        // Add contribution from density matrix
        Vector3Int matrixPos = Vector3Int.FloorToInt(obj.transform.position / tileSize);
        densityMatrix[matrixPos.x, matrixPos.z] = true;
    }
    public void Remove(GameObject obj)
    {
        list.Remove(obj);
        // Remove contribution from density matrix
        Vector3Int matrixPos = Vector3Int.FloorToInt(obj.transform.position / tileSize);
        densityMatrix[matrixPos.x, matrixPos.z] = false;
    }
    public void ChangeDensityValue(Vector3 position, bool value)
    {
        Vector3Int matrixPos = Vector3Int.FloorToInt(position / tileSize);
        densityMatrix[matrixPos.x, matrixPos.z] = value;
    }
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
}