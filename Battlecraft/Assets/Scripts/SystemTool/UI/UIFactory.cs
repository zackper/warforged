using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace SystemToolUI
{
    public enum Type
    {
        SLIDER,
        CHECKBOX,
        NUMBER_INPUT,
        VECTOR_INPUT,
        RADIAL_BUTTONS,
        RADIAL_BUTTONS4,
        CIRCULAR_BUTTON,
        CATEGORY_TITLE,
        SELECTED_TEXTURE,
        SELECTED_OBJECT_COLLECTION,
        RECENTLY_USED,
        ASSET_COLLECTION,
        ASSET_LIST,
        ASSET_BUTTON,
        COLOR_PICKER
    }

    public class UIFactoryHelper
    {
        public static Slider CreateSlider(string name, float minValue, float maxValue, float defaultValue)
        {
            // Add Slider for size
            GameObject sliderObj = UIFactory.Instance.Create(Type.SLIDER);
            Slider slider = sliderObj.GetComponent<Slider>();
            slider.titleLabel.text = name;
            slider.minValue = minValue;
            slider.maxValue = maxValue;
            slider.value = defaultValue;
            return slider;
        }
        public static Checkbox CreateCheckbox(string name, bool isOn)
        {
            // Add Slider for size
            GameObject checkboxObj = UIFactory.Instance.Create(Type.CHECKBOX);
            Checkbox checkbox = checkboxObj.GetComponent<Checkbox>();
            checkbox.title = name;
            checkbox.isOn = isOn;
            checkbox.onToggleChange.Invoke(isOn);
            return checkbox;
        }
        public static VectorInputField CreateVectorInputField(string name1, string name2, string name3, float increamentAmount, string postfix = "", string format = "F0")
        {
            GameObject obj = UIFactory.Instance.Create(Type.VECTOR_INPUT);
            VectorInputField vectorInputField = obj.GetComponent<VectorInputField>();

            vectorInputField.x.titleLabel.text = name1;
            vectorInputField.y.titleLabel.text = name2;
            vectorInputField.z.titleLabel.text = name3;

            vectorInputField.increamentAmount = increamentAmount;
            vectorInputField.postfix = postfix;
            vectorInputField.format = format;

            return vectorInputField;
        }
        public static ColorPicker CreateColorPicker(string name, Color defaultColor)
        {
            GameObject colorPickerObj = UIFactory.Instance.Create(Type.COLOR_PICKER);
            ColorPicker colorPicker = colorPickerObj.GetComponent<ColorPicker>();

            colorPicker.color = defaultColor;

            return colorPicker;
        }
        public static GameObject CreateCategory(string name)
        {
            GameObject categoryObj = UIFactory.Instance.Create(Type.CATEGORY_TITLE);
            TMP_Text categoryTitle = categoryObj.GetComponent<TMP_Text>();
            categoryTitle.text = name;
            return categoryObj;
        }
    }


    public class UIFactory : MonoBehaviour
    {
        // References to prefab elements
        [SerializeField]
        private GameObject sliderPrefeb;
        [SerializeField]
        private GameObject checkboxPrefab;
        [SerializeField]
        private GameObject radialButtons4Prefab;
        [SerializeField]
        private GameObject radialButtonsPrefab;
        [SerializeField]
        private GameObject circularButton;
        [SerializeField]
        private GameObject numberInputPrefab;
        [SerializeField]
        private GameObject vectorInputPrefab;
        [SerializeField]
        private GameObject categoryTitlePrefab;
        [SerializeField]
        private GameObject selectedTexturePrefab;
        [SerializeField]
        private GameObject selectedObjectCollectionPrefab;
        [SerializeField]
        private GameObject recentlyUsedPrefab;
        [SerializeField]
        private GameObject assetCollection;
        [SerializeField]
        private GameObject assetList;
        [SerializeField]
        private GameObject assetButton;
        [SerializeField]
        private GameObject colorPicker;

        // Dictionary used as dispatcher using types
        private Dictionary<Type, GameObject> dispatcher = new Dictionary<Type, GameObject>();

        // Initialize dispatcher
        private void Start()
        {
            dispatcher.Add(Type.SLIDER, sliderPrefeb);
            dispatcher.Add(Type.CHECKBOX, checkboxPrefab);
            dispatcher.Add(Type.RADIAL_BUTTONS, radialButtonsPrefab);
            dispatcher.Add(Type.RADIAL_BUTTONS4, radialButtons4Prefab);
            dispatcher.Add(Type.CIRCULAR_BUTTON, circularButton);
            dispatcher.Add(Type.NUMBER_INPUT, numberInputPrefab);
            dispatcher.Add(Type.VECTOR_INPUT, vectorInputPrefab);
            dispatcher.Add(Type.CATEGORY_TITLE, categoryTitlePrefab);
            dispatcher.Add(Type.SELECTED_TEXTURE, selectedTexturePrefab);
            dispatcher.Add(Type.SELECTED_OBJECT_COLLECTION, selectedObjectCollectionPrefab);
            dispatcher.Add(Type.RECENTLY_USED, recentlyUsedPrefab);
            dispatcher.Add(Type.ASSET_COLLECTION, assetCollection);
            dispatcher.Add(Type.ASSET_LIST, assetList);
            dispatcher.Add(Type.ASSET_BUTTON, assetButton);
            dispatcher.Add(Type.COLOR_PICKER, colorPicker);
        }

        // Public function to create UI elements
        public GameObject Create(Type type)
        {
            return Instantiate(dispatcher[type]);
        }
        public GameObject Create(Type type, Transform parent)
        {
            return Instantiate(dispatcher[type], parent);
        }


        #region SINGLETON
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        // Singleton Vars
        public static UIFactory Instance { get; private set; }
        private void Awake()
        {
            if (Instance != null && Instance != this)
            {
                Destroy(this);
            }
            else
            {
                Instance = this;
            }
        }
        #endregion
    }
}
