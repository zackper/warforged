using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using SystemToolUI;

namespace SystemToolUI
{
    public class RadialButtons : MonoBehaviour
    {
        public TMP_Text titleLabel;
        public List<RadialButton> buttons = new List<RadialButton>();

        public Color selectedColor;
        public Color unselectedColor;

        [SerializeField]
        private GameObject gridLayout;

        // Does not set image of button. Caller must add it.
        public RadialButton AddButton()
        {
            // Create button
            GameObject buttonObj = UIFactory.Instance.Create(Type.CIRCULAR_BUTTON);
            RadialButton button = buttonObj.GetComponent<RadialButton>();
            button.color = unselectedColor;
            // Add it to gridlayout
            button.transform.SetParent(gridLayout.transform);
            button.onButtonClick.AddListener(delegate
            {
                foreach (RadialButton button2 in buttons)
                    button2.color = unselectedColor;

                button.color = selectedColor;
            });
            buttons.Add(button);


            return button;
        }
    }
}

