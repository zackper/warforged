using UnityEngine;
using UnityEngine.Events;
using TMPro;

namespace SystemToolUI
{
    public class NumberInputField : MonoBehaviour
    {
        public TMP_Text titleLabel;
        public float    _value;
        public float    value
        {
            get
            {
                return _value;
            }
            set
            {
                _value = value;
                inputField.text = value.ToString(format) + postfix;
            }
        }
        public float    increamentAmount = 5;
        public string   format = "F0";
        public string   postfix = "";

        // Reference to input field and redirect event
        [SerializeField]
        private TMP_InputField  inputField;
        public UnityEvent       onValueChanged = new UnityEvent();

        // Button Events. It could be one parameterized function but its clearer this way.
        public void Increament()
        {
            value += increamentAmount;
        }
        public void Decreament()
        {
            value -= increamentAmount;
        }

        // Initialize and preconditions
        private void Start()
        {
            Debug.Assert(inputField);
            Debug.Assert(onValueChanged != null);

            inputField.onValueChanged.AddListener(delegate {
                inputField.text = _value.ToString(format) + postfix;
                onValueChanged.Invoke();
            });
            inputField.text = _value.ToString(format) + postfix;
        }
    }
}