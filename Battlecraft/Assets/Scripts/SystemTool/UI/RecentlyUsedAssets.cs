using UnityEngine;
using UnityEngine.Events;

namespace SystemToolUI
{
    public class RecentlyUsedAssets : MonoBehaviour
    {
        [SerializeField]
        private AssetList list;

        public void Add(AssetData data, UnityAction<AssetData> onClick)
        {
            foreach(AssetButton button in list.buttons)
            {
                if(button.data.name == data.name)
                {
                    list.Remove(data);
                    break;
                }
            }

            // Add first and add listener
            list.Add(data, 0);
            AssetButton b = list.buttons[0];
            b.onClick.AddListener(delegate{ onClick.Invoke(b.data); });

            if (list.buttons.Count > 5)
                list.buttons.RemoveAt(list.buttons.Count - 1);
        }
    }
}

