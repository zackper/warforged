using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


namespace SystemToolUI
{
    public class RadialButtons4 : MonoBehaviour
    {
        public TextMeshProUGUI titleLabel;
        public List<RadialButton> buttons = new List<RadialButton>();

        public Color selectedColor;
        public Color unselectedColor;

        private void Start()
        {
            foreach(RadialButton button in buttons)
            {
                button.onButtonClick.AddListener(delegate
                {
                    foreach (RadialButton button2 in buttons)
                        if (button2 == button)
                            button2.color = selectedColor;
                        else
                            button2.color = unselectedColor;
                });
            }
        }
    }
}
