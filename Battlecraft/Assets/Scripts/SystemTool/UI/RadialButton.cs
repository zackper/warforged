using UnityEngine.Events;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace SystemToolUI
{
    [RequireComponent(typeof(Button))]
    public class RadialButton : MonoBehaviour
    {
        public TextMeshProUGUI title;
        public Color color
        {
            get
            {
                return button.GetComponent<Image>().color;
            }
            set
            {
                button.GetComponent<Image>().color = value;
            }
        }

        private UnityEngine.UI.Button button;
        public UnityEvent onButtonClick = new UnityEvent();

        private void Awake()
        {
            button = GetComponent<Button>();
            button.onClick.AddListener(delegate { onButtonClick.Invoke(); });
        }
    }
}
