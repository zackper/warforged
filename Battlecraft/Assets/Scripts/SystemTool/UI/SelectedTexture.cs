using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using TMPro;

namespace SystemToolUI
{
    [RequireComponent(typeof(RawImage))]
    public class SelectedTexture : MonoBehaviour
    {
        public TMP_Text titleLabel;
        public TMP_Text selectedName;
        public RawImage selectedImage;
        public UnityEvent onImageChange;

        public AssetData data;

        private void Awake()
        {
            AssetLoader loader = new AssetLoader("textures/terrain");
            data = loader.assetsData[0];
        }

        public void OnButtonClick()
        {
            GameObject canvas = GameObject.Find("Canvas");
            GameObject assetCollectionObj = UIFactory.Instance.Create(Type.ASSET_COLLECTION, canvas.transform);
            AssetCollection assetCollection = assetCollectionObj.GetComponent<AssetCollection>();
            assetCollection.buttonClick.AddListener(delegate (AssetData data)
            {
                this.data = data;
                selectedImage.texture = data.texture;
                onImageChange.Invoke();
                assetCollection.Close();
            });

            assetCollection.Load("textures/terrain");
        }
    }
}
