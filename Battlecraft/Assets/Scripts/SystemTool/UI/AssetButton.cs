using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

namespace SystemToolUI
{
    [RequireComponent(typeof(RawImage))]
    [RequireComponent(typeof(Button))]
    public class AssetButton : MonoBehaviour
    {
        // OnClick subscription
        public AssetData    data;
        public RawImage     image;
        public UnityEvent   onClick = new UnityEvent();

        // Private components
        private Button button;

        private void Awake()
        {
            // Get self references
            image = GetComponent<RawImage>();
            button = GetComponent<Button>();
            // Redirect button callback
            button.onClick.AddListener(delegate { onClick.Invoke(); });
        }

        public void Init(AssetData data)
        {
            this.data = data;
            image.texture = data.texture;
        }
    }
}

