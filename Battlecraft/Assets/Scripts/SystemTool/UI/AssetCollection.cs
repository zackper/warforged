using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SystemToolUI;
using UnityEngine.Events;

namespace SystemToolUI
{
    public class AssetCollection : MonoBehaviour
    {
        public UnityEvent<AssetData> buttonClick = new UnityEvent<AssetData>();

        // Populate AssetListPrefab here
        [SerializeField]
        private GameObject assetContainer;
        // Private dictionary to categorize loaded assets
        private Dictionary<string, List<AssetData>> categories = new Dictionary<string, List<AssetData>>();

        public void Load(string bundleName, List<string> filteredTags = null)
        {
            AssetLoader loader = new AssetLoader(bundleName);

            // Add loaded data to dictionary of categories
            foreach(AssetData data in loader.assetsData)
            {
                if (filteredTags != null && filteredTags.Contains(data.mainTag) == false)
                    continue;

                if (categories.ContainsKey(data.category) == false)
                    categories.Add(data.category, new List<AssetData>());
                categories[data.category].Add(data);
            }

            // Create AssetList and subscribe to asset buttons event
            foreach(KeyValuePair<string, List<AssetData>> pair in categories)
            {
                GameObject listObj = UIFactory.Instance.Create(Type.ASSET_LIST);
                AssetList list = listObj.GetComponent<AssetList>();
                list.titleLabel.text = pair.Key;
                foreach (AssetData data in pair.Value)
                {
                    list.Add(data);
                }
                foreach(AssetButton button in list.buttons)
                {
                    button.onClick.AddListener(delegate
                    {
                        buttonClick.Invoke(button.data);
                    });
                }

                listObj.transform.SetParent(assetContainer.transform);
            }
        }

        public void Close()
        {
            Destroy(this.gameObject);
        }
    }
}
