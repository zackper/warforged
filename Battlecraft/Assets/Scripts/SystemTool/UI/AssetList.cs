using System.Collections.Generic;
using UnityEngine;
using TMPro;
using SystemToolUI;

namespace SystemToolUI
{
    public class AssetList : MonoBehaviour
    {
        public TMP_Text titleLabel;
        public List<AssetButton> buttons = new List<AssetButton>();

        [SerializeField]
        private GameObject gridLayout;
    
        public void Add(AssetData data, int index = -1)
        {
            GameObject buttonObj = UIFactory.Instance.Create(Type.ASSET_BUTTON, gridLayout.transform);
            AssetButton button = buttonObj.GetComponent<AssetButton>();
            button.Init(data);
            if (index != -1) { // Change sibling index
                buttons.Insert(index, button);
                buttonObj.transform.SetSiblingIndex(index);
            }
            else
                buttons.Add(button);

        }
        public void Remove(AssetData data)
        {
            foreach (AssetButton button in buttons)
            {
                if(button.data.name == data.name)
                {
                    buttons.Remove(button);
                    button.transform.SetParent(null);
                    Destroy(button.gameObject);
                    return;
                }
            }
        }
    }
}
