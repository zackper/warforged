using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
[RequireComponent(typeof(Image))]
public class ToggleButton : MonoBehaviour
{
    [SerializeField]
    private Color activatedColor;
    [SerializeField]
    private Color deactivatedColor;

    public bool isToggled = false;
    public UnityEvent onToggleOn;
    public UnityEvent onToggleOff;

    private void Awake()
    {
        Button button = GetComponent<Button>();
        Image image = GetComponent<Image>();

        button.onClick.AddListener(delegate
        {
            isToggled = !isToggled;
            if (isToggled)
            {
                image.color = activatedColor;
                onToggleOn.Invoke();
            }
            else
            {
                image.color = deactivatedColor;
                onToggleOff.Invoke();
            }
        });
    }
}
