using UnityEngine;
using UnityEngine.Events;
using TMPro;

namespace SystemToolUI
{
    public class Slider : MonoBehaviour
    {
        public TMP_Text titleLabel;
        public TMP_Text valueLabel;
        public float value
        {
            get
            {
                return slider.value;
            }
            set
            {
                slider.value = value;
            }
        }
        public string postfix = "";
        public string format = "F1";

        public float minValue
        {
            get
            {
                return slider.minValue;
            }
            set
            {
                slider.minValue = value;
            }
        }
        public float maxValue
        {
            get
            {
                return slider.maxValue;
            }
            set
            {
                slider.maxValue = value;
            }
        }

        // Handle Unity's slider value change event
        [SerializeField]
        private UnityEngine.UI.Slider slider;
        public UnityEvent onValueChanged = new UnityEvent();

        // Precondition checking
        private void Start()
        {
            Debug.Assert(slider);
            Debug.Assert(onValueChanged != null);

            slider.onValueChanged.AddListener(delegate {
                valueLabel.text = value.ToString(format) + postfix;
                onValueChanged.Invoke();
            });

            valueLabel.text = value.ToString(format) + postfix;
        }
    }
}
