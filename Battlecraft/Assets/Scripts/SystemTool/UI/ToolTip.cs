using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ToolTip : MonoBehaviour
{
    [SerializeField]
    private ToggleButton grid;
    [SerializeField]
    private ToggleButton topdownCamera;

    public static UnityEvent<bool> onGridToggle = new UnityEvent<bool>();
    public static UnityEvent<bool> onCameraToggle = new UnityEvent<bool>();

    private void Awake()
    {
        grid.onToggleOn.AddListener(delegate
        {
            onGridToggle.Invoke(true);
        });
        grid.onToggleOff.AddListener(delegate
        {
            onGridToggle.Invoke(false);
        });

        topdownCamera.onToggleOn.AddListener(delegate
        {
            CameraManager.Instance.ChangeCamera(CameraType.TOPDOWN);
        });
        topdownCamera.onToggleOff.AddListener(delegate
        {
            CameraManager.Instance.ChangeCamera(CameraType.FLY);
        });

        //onGridToggle.Invoke(grid.isToggled);
        //onCameraToggle.Invoke(grid.isToggled);
    }
}