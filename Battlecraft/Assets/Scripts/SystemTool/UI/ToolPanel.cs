using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ToolPanel : MonoBehaviour
{
    
    public string title {
        get
        {
            return _title.text;
        }
        set
        {
            _title.text = value;
        }
    }
    
    [SerializeField]
    private TMP_Text _title;
    [SerializeField]
    private GameObject _uiElementsContainer;

    // Private lit of ui elements
    private List<GameObject> _uiElements = new List<GameObject>();

    private Animator animator;

    public void Add(GameObject uiElement)
    {
        // Set self as parent
        uiElement.transform.SetParent(_uiElementsContainer.transform);
        uiElement.transform.localScale = Vector3.one;
        // Then add it to end of list
        _uiElements.Add(uiElement);
    }
    public void Remove(GameObject uiElement)
    {
        uiElement.transform.SetParent(null);
        _uiElements.Remove(uiElement);
    }
    public void Clear()
    {
        for(int i = _uiElements.Count - 1; i >= 0; i--)
        {
            GameObject uiElement = _uiElements[i];
            Remove(uiElement);
        }
    }

    private void Start()
    {
        animator = GetComponent<Animator>();
        InputManager.Instance.Subscribe("tab", () =>
        {
            SetExpanded(!animator.GetBool("isExpanded"));
        });
    }
    public void SetExpanded(bool isExpanded)
    {
        animator.SetBool("isExpanded", isExpanded);
    }
}
