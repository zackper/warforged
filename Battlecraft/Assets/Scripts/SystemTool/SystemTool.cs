using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface SystemTool
{
    void Initialize(TerrainMesh terrain);
    void GenerateUI();
    void Start();
    void Stop();
    void Update();
    void PopulateToolPanel(ToolPanel toolPanel);
}
