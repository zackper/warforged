using System.Collections.Generic;
using UnityEngine;
using SystemToolUI;
using TMPro;


public class TerrainSculptTool : SystemTool
{
    // List of UI elements
    private List<GameObject> uiElements = new List<GameObject>();
    // Reference to terrain
    private TerrainMesh terrain;
    // Delegate function for sculpting function
    private delegate void SculptFunction(Vector3 point);
    private SculptFunction sculptFunction;
    // Brush for scuplting
    private Brush brush;
    // Keep track of mouse pos
    private Vector3 prevMousePos;

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // Interface functions
    public void Initialize(TerrainMesh terrain)
    {
        // Set terrain reference
        this.terrain = terrain;

        // Set default sculpt to add.
        sculptFunction = Sculpt_Noise;
        // Initialize Brush
        brush = new Brush();
        brush.properties.Add("str", 1);
        brush.properties.Add("strMod", 0.1f);
        brush.properties.Add("height", 1f);
        brush.properties.Add("roughness", 1f);
    }
    public void GenerateUI()
    {
        // Create First Category
        GameObject firstCategoryObj = UIFactory.Instance.Create(Type.CATEGORY_TITLE);
        firstCategoryObj.GetComponent<TMP_Text>().text = "Terrain Size";
        uiElements.Add(firstCategoryObj);

        // Input field for sublevel
        GameObject subLevelFieldObj = UIFactory.Instance.Create(Type.NUMBER_INPUT);
        NumberInputField subLevelField = subLevelFieldObj.GetComponent<NumberInputField>();
        subLevelField.titleLabel.text = "Terrain Detail";
        subLevelField.format = "F0";
        subLevelField.value = terrain.subLevel;
        subLevelField.increamentAmount = 1;
        subLevelField.onValueChanged.AddListener(delegate { terrain.subLevel = (int)subLevelField.value; });
        uiElements.Add(subLevelFieldObj);
        // Input field for Width
        GameObject terrainSizeObj = UIFactory.Instance.Create(Type.NUMBER_INPUT);
        NumberInputField terrainSizeField = terrainSizeObj.GetComponent<NumberInputField>();
        terrainSizeField.titleLabel.text = "Terrain Size";
        terrainSizeField.format = "F0";
        terrainSizeField.postfix = "ft";
        terrainSizeField.value = (int)terrain.GetTrueSize().x;
        // Add listeners
        terrainSizeField.onValueChanged.AddListener(delegate {
            terrain.sizeX = (int)terrainSizeField.value;
            terrain.sizeZ = (int)terrainSizeField.value;
            terrain.updateTerrain.Invoke();
        });
        uiElements.Add(terrainSizeObj);

        // Create second Category
        GameObject secondCategoryObj = UIFactory.Instance.Create(Type.CATEGORY_TITLE);
        secondCategoryObj.GetComponent<TMP_Text>().text = "Brush Settings";
        uiElements.Add(secondCategoryObj);

        // Add 5 radial buttons
        GameObject radialButtonsObj = UIFactory.Instance.Create(Type.RADIAL_BUTTONS);
        RadialButtons radialButtons = radialButtonsObj.GetComponent<RadialButtons>();
        RadialButton add = radialButtons.AddButton();
        add.onButtonClick.AddListener(delegate
        {
            sculptFunction = Sculpt_Add;
        });
        RadialButton subtrack = radialButtons.AddButton();
        subtrack.onButtonClick.AddListener(delegate
        {
            sculptFunction = Sculpt_Subtract;
        });
        RadialButton smooth = radialButtons.AddButton();
        smooth.onButtonClick.AddListener(delegate
        {
            sculptFunction = Sculpt_Smooth;
        });
        RadialButton mountain = radialButtons.AddButton();
        mountain.onButtonClick.AddListener(delegate
        {
            sculptFunction = Sculpt_Noise;
        });
        uiElements.Add(radialButtonsObj);

        // Add Slider for radius
        GameObject radiusSliderObj = UIFactory.Instance.Create(Type.SLIDER);
        Slider radiusSlider = radiusSliderObj.GetComponent<Slider>();
        radiusSlider.titleLabel.text = "Radius";
        radiusSlider.format = "F1";
        radiusSlider.postfix = "m";
        radiusSlider.minValue = 0;
        radiusSlider.maxValue = 50;
        radiusSlider.value = 4f;
        radiusSlider.onValueChanged.AddListener(delegate {
            brush.properties["radius"] = radiusSlider.value;
        });
        brush.properties["radius"] = radiusSlider.value;
        uiElements.Add(radiusSliderObj);
        // Add Slider for size
        GameObject strengthSliderObj = UIFactory.Instance.Create(Type.SLIDER);
        Slider strengthSlider = strengthSliderObj.GetComponent<Slider>();
        strengthSlider.titleLabel.text = "Strength";
        strengthSlider.format = "F2";
        strengthSlider.minValue = 0;
        strengthSlider.maxValue = 10f;
        strengthSlider.value = 1f;
        strengthSlider.onValueChanged.AddListener(delegate {
            brush.properties["str"] = strengthSlider.value;
        });
        brush.properties["str"] = strengthSlider.value;
        uiElements.Add(strengthSliderObj);


        Slider heightSlider = UIFactoryHelper.CreateSlider("Height", 0, 10, 1);
        heightSlider.format = "F1";
        heightSlider.onValueChanged.AddListener(delegate
        {
            brush.properties["height"] = heightSlider.value;
        });
        uiElements.Add(heightSlider.gameObject);

        Slider roughnessSlider = UIFactoryHelper.CreateSlider("Roughness", 0.1f, 4, 3);
        roughnessSlider.format = "F2";
        roughnessSlider.onValueChanged.AddListener(delegate
        {
            brush.properties["roughness"] = roughnessSlider.value;
        });
        uiElements.Add(roughnessSlider.gameObject);

        add.onButtonClick.AddListener(delegate
        {
            heightSlider.gameObject.SetActive(false);
            roughnessSlider.gameObject.SetActive(false);
        });
        subtrack.onButtonClick.AddListener(delegate
        {
            heightSlider.gameObject.SetActive(false);
            roughnessSlider.gameObject.SetActive(false);
        });
        smooth.onButtonClick.AddListener(delegate
        {
            heightSlider.gameObject.SetActive(false);
            roughnessSlider.gameObject.SetActive(false);
        });
        mountain.onButtonClick.AddListener(delegate
        {
            heightSlider.gameObject.SetActive(true);
            roughnessSlider.gameObject.SetActive(true);
        });

    }
    public void Start()
    {
        brush.CreateCursor();
    }
    public void Stop()
    {
        brush.DeleteCursor();
    }
    public void Update()
    {
        if (terrain.RaycastTerrain(Input.mousePosition, out RaycastHit hitInfo, false, LayerMask.GetMask("Terrain"))) {
            // Return if mouse hasnt moved
            if (prevMousePos == Input.mousePosition)
                return;
            prevMousePos = Input.mousePosition;

            brush.DrawCursor(hitInfo.point + new Vector3(0, 0.1f, 0));
            if (Input.GetMouseButton(0))
            {
                sculptFunction(hitInfo.point);
            }
            else if (Input.GetMouseButtonUp(0))
                terrain.UpdateCollider();
        }
    }
    public void PopulateToolPanel(ToolPanel toolPanel)
    {
        foreach (GameObject uiElement in uiElements)
            toolPanel.Add(uiElement);
    }
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // Function params are due to changes. Just wanted to test the algorithms of sculpt
    // Different Sculpt
    private void Sculpt_Add(Vector3 point)
    {
        //Assumes for now that point space == mesh.vertices space.
        Vector3[] vertices = terrain.GetVerticesRef();

        List<Tile> tiles = GridManager.Instance.grid.GetTilesInRadius(point, brush.properties["radius"]);

        foreach (Tile tile in tiles)
        {
            for(int i = 0; i < tile.vertices.Count; i++)
            {
                int index = tile.vertices[i];
                if(Vector3.Distance(point, vertices[index]) < brush.properties["radius"])
                {
                    float height = vertices[index].y + brush.properties["strMod"] * brush.properties["str"];

                    // Add its value as height for vertex height;
                    float distance = Vector2.Distance(new Vector2(point.x, point.z), new Vector2(vertices[index].x, vertices[index].z));
                    float distanceMutliplier = Mathf.Log(brush.properties["radius"] - distance, 4) / Mathf.Log(brush.properties["radius"], 4);

                    Vector3 newPos = new Vector3(vertices[index].x, height, vertices[index].z);

                    if (distanceMutliplier < 0)
                        distanceMutliplier = 0;
                    vertices[index] = vertices[index] * (1 - distanceMutliplier) + newPos * distanceMutliplier;
                }
            }
        }

        terrain.UpdateMesh();
        GameObjectManager.Instance.UpdateObjectsHeightInTiles(terrain, tiles);
    }
    private void Sculpt_Subtract(Vector3 point)
    {
        //Assumes for now that point space == mesh.vertices space.
        Vector3[] vertices = terrain.GetVerticesRef();

        List<Tile> tiles = GridManager.Instance.grid.GetTilesInRadius(point, brush.properties["radius"]);

        foreach (Tile tile in tiles)
        {
            for (int i = 0; i < tile.vertices.Count; i++)
            {
                int index = tile.vertices[i];
                if (Vector3.Distance(point, vertices[index]) < brush.properties["radius"])
                {
                    float height = vertices[index].y - brush.properties["strMod"] * brush.properties["str"];

                    // Add its value as height for vertex height;
                    float distance = Vector2.Distance(new Vector2(point.x, point.z), new Vector2(vertices[index].x, vertices[index].z));
                    float distanceMutliplier = Mathf.Log(brush.properties["radius"] - distance, 4) / Mathf.Log(brush.properties["radius"], 4);

                    Vector3 newPos = new Vector3(vertices[index].x, height, vertices[index].z);

                    if (distanceMutliplier < 0)
                        distanceMutliplier = 0;
                    vertices[index] = vertices[index] * (1 - distanceMutliplier) + newPos * distanceMutliplier;
                }
            }
        }

        terrain.UpdateMesh();
        GameObjectManager.Instance.UpdateObjectsHeightInTiles(terrain, tiles);
    }
    private void Sculpt_Flatten(Vector3 point)
    {
        Vector3[] vertices = terrain.GetVerticesRef();
        // Find affected points.
        List<int> affectedIndices = new List<int>();
        // Keep track of closest vertex.
        float closestDistance = 1000;
        int closestIndex = -1;
        for (int i = 0; i < vertices.Length; i++)
        {
            float distance = Vector3.Distance(vertices[i], point);
            if (distance < brush.properties["radius"])
            {
                affectedIndices.Add(i);
                if(distance < closestDistance)
                {
                    closestDistance = distance;
                    closestIndex = i;
                }
            }
        }

        // Fletten
        foreach(int i in affectedIndices)
        {
            vertices[i] = new Vector3(vertices[i].x, vertices[closestIndex].y, vertices[i].z);
        }
        terrain.UpdateMesh();
    }
    private void Sculpt_Smooth(Vector3 point)
    {
        Vector3[] vertices = terrain.GetVerticesRef();
        // Find affected points.
        List<int> affectedIndices = new List<int>();

        float averageHeight = 0;
        List<Tile> tiles = GridManager.Instance.grid.GetTilesInRadius(point, brush.properties["radius"]);
        foreach(Tile tile in tiles)
        {
            for(int i = 0; i < tile.vertices.Count; i++)
            {
                int index = tile.vertices[i];
                Vector3 vertex = vertices[index];
                if (Vector3.Distance(vertex, point) < brush.properties["radius"])
                {
                    affectedIndices.Add(index);
                    averageHeight += vertices[i].y;
                }
            }
        }        
        averageHeight /= affectedIndices.Count;

        // Smooth with ~gausian cut off
        foreach (int index in affectedIndices)
        {
            // Add its value as height for vertex height;
            float distance = Vector2.Distance(new Vector2(point.x, point.z), new Vector2(vertices[index].x, vertices[index].z));
            float distanceMutliplier = Mathf.Log(brush.properties["radius"] - distance, 4) / Mathf.Log(brush.properties["radius"], 4);

            Vector3 newPos = new Vector3(0, (averageHeight - vertices[index].y) * brush.properties["strMod"] * brush.properties["str"], 0);
            if (distanceMutliplier < 0)
                distanceMutliplier = 0;
            vertices[index] += newPos * distanceMutliplier;
            //vertices[index] += new Vector3(0, (averageHeight - vertices[index].y) * brush.properties["strMod"] * brush.properties["str"], 0);
        }

        terrain.UpdateMesh();
        GameObjectManager.Instance.UpdateObjectsHeightInTiles(terrain, tiles);
    }
    private void Sculpt_Noise(Vector3 point)
    {
        //Assumes for now that point space == mesh.vertices space.
        Vector3[] vertices = terrain.GetVerticesRef();

        List<Tile> tiles = GridManager.Instance.grid.GetTilesInRadius(point, brush.properties["radius"]);

        foreach (Tile tile in tiles)
        {
            for (int i = 0; i < tile.vertices.Count; i++)
            {
                int index = tile.vertices[i];
                if (Vector3.Distance(point, vertices[index]) < brush.properties["radius"])
                {
                    // Find relative point in height map
                    Vector3 vertexLocalPos = vertices[index] - (point - Vector3.one * brush.properties["radius"]);

                    float height = SampleLayeredNoise(vertices[index].x * 0.1f, vertices[index].z * 0.1f);
                    height *= brush.properties["height"];

                    // Add its value as height for vertex height;
                    float distance = Vector2.Distance(new Vector2(point.x, point.z), new Vector2(vertices[index].x, vertices[index].z));
                    float distanceMutliplier = Mathf.Log(brush.properties["radius"] - distance, 4) / Mathf.Log(brush.properties["radius"], 4);

                    Vector3 newPos = new Vector3(vertices[index].x, height, vertices[index].z);

                    if (distanceMutliplier < 0)
                        distanceMutliplier = 0;
                    vertices[index] = vertices[index] * (1 - distanceMutliplier) + newPos * distanceMutliplier;
                }
            }
        }

        terrain.UpdateMesh();
        GameObjectManager.Instance.UpdateObjectsHeightInTiles(terrain, tiles);
    }
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    //Util
    public float SampleLayeredNoise(float x, float y)
    {
        float persistance = 0.4f;

        float noise = Mathf.PerlinNoise(x, y);
        float highFreqNoise = Mathf.PerlinNoise(x * 6, y * 6);
        noise = noise + highFreqNoise * 0.2f;

        float frequency = 1;
        float factor = 1;
        int octaves = 4;
        for (int i = 0; i < octaves; i++)
        {
            float newNoise = Mathf.PerlinNoise(
                x * frequency + i * 0.72354f,
                y * frequency + i * 0.72354f
            );
            noise = noise + newNoise * factor;

            factor *= persistance;
            frequency *= brush.properties["roughness"];
        }

        return noise;
    }
    public double Map(double x, double in_min, double in_max, double out_min, double out_max)
    {
        return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
    }
}
