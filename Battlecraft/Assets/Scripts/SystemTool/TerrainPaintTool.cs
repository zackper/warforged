
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SystemToolUI;
using TMPro;

public class PaintTool
{
    public ComputeShader computeShader;

    public PaintTool()
    {
        computeShader = Resources.Load<ComputeShader>("Compute Shaders/TexturePaintCompute");
    }

    // Functions that copy a texture/Render texture
    public void CopyRtGPU(RenderTexture source, RenderTexture dest)
    {
        if (source.width != dest.width || source.height != dest.height)
            throw new System.Exception();

        // Get kernel index
        int kernel = computeShader.FindKernel("CopyRt");

        // Initialize params of compute shader
        computeShader.SetTexture(kernel, "destRt", dest);
        computeShader.SetTexture(kernel, "sourceRt", source);

        computeShader.Dispatch(kernel, dest.width / 8, dest.height / 8, 1);
    }
    public void CopyTextureGPU(Texture2D source, RenderTexture dest)
    {
        if (source.width != dest.width || source.height != dest.height)
            throw new System.Exception();

        // Get kernel index
        int kernel = computeShader.FindKernel("CopyTexture");

        // Initialize params of compute shader
        computeShader.SetTexture(kernel, "destRt", dest);
        computeShader.SetTexture(kernel, "source", source);

        computeShader.Dispatch(kernel, dest.width / 8, dest.height / 8, 1);
    }
    // Blit function that also uses a mask
    public void BlitGPU(
        Texture2D source,
        RenderTexture dest,
        int sx, int sy,
        int dx, int dy,
        int width, int height,
        float opacity = 1
    )
    {
        // Get kernel index
        int kernel = computeShader.FindKernel("Blit");

        // Initialize params of compute shader
        computeShader.SetTexture(kernel, "destRt", dest);
        computeShader.SetTexture(kernel, "source", source);

        computeShader.SetInt("sx", sx);
        computeShader.SetInt("sy", sy);
        computeShader.SetInt("dx", dx);
        computeShader.SetInt("dy", dy);
        computeShader.SetInt("width", width);
        computeShader.SetInt("height", height);
        computeShader.SetFloat("opacity", opacity);

        computeShader.Dispatch(kernel, dest.width / 8, dest.height / 8, 1);
    }
    public void BlitGPU(
        RenderTexture source,
        RenderTexture dest,
        int sx, int sy,
        int dx, int dy,
        int width, int height,
        float opacity = 1
    )
    {
        // Get kernel index
        int kernel = computeShader.FindKernel("BlitRt");

        // Initialize params of compute shader
        computeShader.SetTexture(kernel, "destRt", dest);
        computeShader.SetTexture(kernel, "sourceRt", source);

        computeShader.SetInt("sx", sx);
        computeShader.SetInt("sy", sy);
        computeShader.SetInt("dx", dx);
        computeShader.SetInt("dy", dy);
        computeShader.SetInt("width", width);
        computeShader.SetInt("height", height);
        computeShader.SetFloat("opacity", opacity);

        computeShader.Dispatch(kernel, dest.width / 8, dest.height / 8, 1);
    }
    public void MaskedBlitGPU(
        Texture2D source,
        RenderTexture dest,
        Texture2D mask,
        int sx, int sy,
        int dx, int dy,
        int width, int height,
        float opacity = 1
    )
    {
        // Get kernel index
        int kernel = computeShader.FindKernel("MaskedBlit");

        // Initialize params of compute shader
        computeShader.SetTexture(kernel, "destRt", dest);
        computeShader.SetTexture(kernel, "source", source);
        computeShader.SetTexture(kernel, "mask", mask);

        computeShader.SetInt("sx", sx);
        computeShader.SetInt("sy", sy);
        computeShader.SetInt("dx", dx);
        computeShader.SetInt("dy", dy);
        computeShader.SetInt("width", width);
        computeShader.SetInt("height", height);
        computeShader.SetFloat("opacity", opacity);

        computeShader.Dispatch(kernel, dest.width / 8, dest.height / 8, 1);
    }
    public void OpacityClampedMaskedBlitGPU(
        Texture2D source,
        RenderTexture dest,
        Texture2D mask,
        int sx, int sy,
        int dx, int dy,
        int width, int height,
        float opacity = 1
    )
    {
        // Get kernel index
        int kernel = computeShader.FindKernel("OpacityClampedMaskedBlit");

        // Initialize params of compute shader
        computeShader.SetTexture(kernel, "destRt", dest);
        computeShader.SetTexture(kernel, "source", source);
        computeShader.SetTexture(kernel, "mask", mask);

        computeShader.SetInt("sx", sx);
        computeShader.SetInt("sy", sy);
        computeShader.SetInt("dx", dx);
        computeShader.SetInt("dy", dy);
        computeShader.SetInt("width", width);
        computeShader.SetInt("height", height);
        computeShader.SetFloat("opacity", opacity);

        computeShader.Dispatch(kernel, dest.width / 8, dest.height / 8, 1);
    }

    // Draw grid on texture
    public void DrawGridGPU(RenderTexture dest, int tileSize, int lineThickness)
    {
        // Get kernel index
        int kernel = computeShader.FindKernel("DrawGrid");
        // Initialize params of compute shader
        computeShader.SetTexture(kernel, "destRt", dest);
        computeShader.SetInt("tileSize", tileSize);
        computeShader.SetInt("lineThickness", lineThickness);

        computeShader.Dispatch(kernel, dest.width / 8, dest.height / 8, 1);
    }

    // Simple Blit function (CPU, slow)
    public static void Blit(
        Texture2D source,
        Texture2D dest,
        int sx, int sy,
        int dx, int dy,
        int width, int height
    )
    {
        Color[] sourcePixels = source.GetPixels(sx, sy, width, height);
        dest.SetPixels(dx, dy, width, height, sourcePixels);
    }
    // Blit function that also uses a mask (CPU, slow)
    public static void MaskedBlit(
        Texture2D source,
        Texture2D dest,
        Texture2D mask,
        int sx, int sy,
        int dx, int dy,
        int width, int height
    )
    {
        Texture2D resizedMask = Resize(mask, width, height);

        // Final pixels at dx dy will be the lerp result using maskedPixels as lerp indicator
        //source.GetPixels();
        Color[] sourcePixels = source.GetPixels(sx, sy, width, height);
        Color[] destPixels = dest.GetPixels(dx, dy, width, height);
        Color[] maskPixels = resizedMask.GetPixels(0, 0, width, height);

        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                int index = i * width + j;
                destPixels[index] = Color.Lerp(
                    destPixels[index],
                    sourcePixels[index],
                    maskPixels[index].a
                );
            }
        }
        // Set final pixels;
        dest.SetPixels(dx, dy, width, height, destPixels);
    }
    // Function that resizes a texture
    public static Texture2D Resize(Texture2D texture, int width, int height)
    {
        RenderTexture rt = new RenderTexture(width, height, 24);
        RenderTexture.active = rt;
        Graphics.Blit(texture, rt);
        Texture2D result = new Texture2D(width, height);
        result.ReadPixels(new Rect(0, 0, width, height), 0, 0);
        result.Apply();

        return result;
    }
    // This function copies source to dest and repeats source to fill whole of dest
    public void CopyRepeating(Texture2D source, Texture2D dest)
    {
        RenderTexture rt = new RenderTexture(dest.width, dest.height, 32);
        rt.enableRandomWrite = true;
        rt.Create();

        // Lets assume source is smaller than dest for now.
        for (int i = 0; i < dest.width; i += source.width)
        {
            for (int j = 0; j < dest.height; j += source.height)
            {
                BlitGPU(
                    source,
                    rt,
                    0, 0,
                    i, j,
                    i + source.width < dest.width ? source.width : dest.width - i,
                    j + source.height < dest.height ? source.height : dest.height - j
                );
            }
        }

        RenderTexture.active = rt;
        dest.ReadPixels(new Rect(0, 0, rt.width, rt.height), 0, 0);
        dest.Apply();
        RenderTexture.active = null;
    }
}

public class TerrainPaintTool : SystemTool
{
    // List of UI elements
    private List<GameObject> uiElements = new List<GameObject>();
    // Create paint tool 
    private PaintTool paintTool = new PaintTool();
    // Reference to terrain
    private TerrainMesh terrain;
    // Brush for scuplting
    private Brush brush;
    // Paint mode
    private enum PaintMode
    {
        GRID,
        BRUSH
    }
    private PaintMode mode = PaintMode.GRID;

    // Main texture of the terrain
    private RenderTexture mainRt;
    // Preview texture for brush cursor
    private RenderTexture previewRt;
    // Draw Buffer texture. On click always starts empty and limits texture to opacity
    private RenderTexture drawBufferRt;
    // Empty texture for quickly emptying textures
    private RenderTexture emptyTexture;
    private Vector3 prevMousePos = new Vector3(-1, -1, -1);
    // Source texture from where Blit is used
    private Texture2D sourceTexture;
    private Texture2D sourceBrushTexture;

    // Bool to decide if the grid should be drawn
    private bool gridEnabled = false;

    // loaded textures from resources
    public Texture2D selectedBrushTexture;


    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // Main functions called on Mouse Events (from update)
    private void DrawCursor(int dx, int dy)
    {
        float radius = brush.properties["radius"];

        // Reset preview texture
        paintTool.CopyRtGPU(mainRt, previewRt);
        // Blit preview
        if (mode == PaintMode.GRID)
        {
            // Set variables to match grid
            int tileSizePx = GetTileSizePixels();
            dx = dx - dx % tileSizePx;
            dy = dy - dy % tileSizePx;
            radius = radius - radius % radius;
        }
        paintTool.MaskedBlitGPU(
            sourceTexture,
            previewRt,
            sourceBrushTexture,
            dx, dy,
            dx, dy,
            (int)radius, (int)radius,
            brush.properties["opacity"]
        );
    }
    private void DrawToPreview(int dx, int dy)
    {
        float radius = brush.properties["radius"];
        float opacity = brush.properties["opacity"];

        if (mode == PaintMode.GRID)
        {
            // Set variables to match grid
            int tileSizePx = GetTileSizePixels();
            dx = dx - dx % tileSizePx;
            dy = dy - dy % tileSizePx;
            radius = radius - radius % radius;
        }

        paintTool.OpacityClampedMaskedBlitGPU(
            sourceTexture,
            drawBufferRt,
            sourceBrushTexture,
            dx, dy,
            dx, dy,
            (int)radius, (int)radius,
            opacity
        );
        // Second, Copy(mainRt, previewRt)
        paintTool.CopyRtGPU(mainRt, previewRt);
        //// Third, Blit whole drawBuffer to previewRt
        paintTool.BlitGPU(
            drawBufferRt,
            previewRt,
            0, 0,
            0, 0,
            previewRt.width, previewRt.height
        );
    }
    private void ApplyPreviewToMain()
    {
        // Copy preview to mainRt and clear drawBufferRt
        ResetPreview();
        DrawToPreview(-1000, -1000);
        paintTool.CopyRtGPU(previewRt, mainRt);
        paintTool.CopyRtGPU(emptyTexture, drawBufferRt);
    }
    public void DrawGrid()
    {
        paintTool.DrawGridGPU(previewRt, GetTileSizePixels(), 2);
    }
    public void ResetPreview()
    {
        paintTool.CopyRtGPU(mainRt, previewRt);
    }
    public int GetTileSizePixels()
    {
        float sizeX = terrain.GetTrueSize().x;      // meters
        float textureSizeX = previewRt.width;       //pixels

        float tileSize = 1.524f;                // One foot - 0.3meters. 5ft square units
        int tileSizePixels = (int)((textureSizeX * tileSize) / sizeX);
        return tileSizePixels;
    }
    // Temporary Draw at middle of screen for adjusting brush variables
    private void DrawBrushCursorAtMiddle()
    {
        if (terrain.RaycastTerrain(new Vector3(Screen.width / 2, Screen.height / 2, 0), out RaycastHit hitInfo, true))
        {
            int radius = (int)brush.properties["radius"];
            // Point of hit - radius/2 to center the hit of the brush
            int dx = (int)(mainRt.width * hitInfo.textureCoord.x) - (int)radius / 2;
            int dy = (int)(mainRt.height * hitInfo.textureCoord.y) - (int)radius / 2;
            DrawCursor(dx, dy);
        }
    }
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // Interface Functions
    public void Initialize(TerrainMesh terrain)
    {
        // Set terrain reference
        this.terrain = terrain;

        // Initialize Brush
        brush = new Brush();
        brush.properties["radius"] = 400;
        brush.properties.Add("str", 1);
        brush.properties.Add("opacity", 1);

        // Get main texture
        Texture2D mainTexture = (Texture2D)terrain.GetComponent<Renderer>().material.mainTexture;

        // Initialize main texture
        mainRt = new RenderTexture(mainTexture.width, mainTexture.height, 32);
        mainRt.enableRandomWrite = true;
        mainRt.Create();
        paintTool.CopyTextureGPU(mainTexture, mainRt);

        // Initialize preview texture
        previewRt = new RenderTexture(mainRt.width, mainRt.height, mainRt.depth);
        previewRt.enableRandomWrite = true;
        previewRt.Create();
        paintTool.CopyTextureGPU(mainTexture, previewRt);

        // Initialize Draw Buffer
        drawBufferRt = new RenderTexture(mainRt.width, mainRt.height, mainRt.depth);
        drawBufferRt.enableRandomWrite = true;
        drawBufferRt.Create();

        // Inititalize empty texture
        emptyTexture= new RenderTexture(mainRt.width, mainRt.height, mainRt.depth);
        emptyTexture.enableRandomWrite = true;
        emptyTexture.Create();

        //Initialize selected texture and mask / brush texture
        AssetLoader loader = new AssetLoader("textures/brush");
        selectedBrushTexture = loader.assetsData[0].texture;
        //selectedBrushTexture.alphaIsTransparency = true;

        // Initialize sourceTexture
        loader = new AssetLoader("textures/terrain");
        sourceTexture = new Texture2D(mainRt.width, mainRt.height);
        SelectTexture(loader.assetsData[0].texture);


        // Init source brush texture
        SelectBrushTexture(selectedBrushTexture);

        // Observer pattern subscriptions
        // Subscribe to grid toggle events
        ToolTip.onGridToggle.AddListener(delegate (bool toggle)
        {
            if (toggle)
            {
                gridEnabled = true;
                this.DrawGrid();
            }
            else
            {
                gridEnabled = false;
                this.ResetPreview();
            }
        });
    }
    public void GenerateUI()
    {
        // Add brush settings category

        // Shape of brush
        GameObject radialButtonsObj = UIFactory.Instance.Create(Type.RADIAL_BUTTONS);
        RadialButtons radialButtons = radialButtonsObj.GetComponent<RadialButtons>();
        RadialButton b1 = radialButtons.AddButton();
        b1.onButtonClick.AddListener(delegate
        {
            mode = PaintMode.BRUSH;
            selectedBrushTexture = new AssetLoader("textures/brush").assetsData[0].texture;
            SelectBrushTexture(selectedBrushTexture);
        });
        RadialButton b2 = radialButtons.AddButton();
        b2.onButtonClick.AddListener(delegate
        {
            mode = PaintMode.GRID;
            selectedBrushTexture = new AssetLoader("textures/brush").assetsData[1].texture;
            SelectBrushTexture(selectedBrushTexture);
        });
        uiElements.Add(radialButtonsObj);

        // Add texture select
        GameObject selectedTextureObj = UIFactory.Instance.Create(Type.SELECTED_TEXTURE);
        SelectedTexture selectedTexture = selectedTextureObj.GetComponent<SelectedTexture>();
        selectedTexture.titleLabel.text = "Texture";
        selectedTexture.onImageChange.AddListener(delegate
        {
            SelectTexture(selectedTexture.data.texture);
        });
        SelectTexture(selectedTexture.data.texture);
        uiElements.Add(selectedTextureObj);

        // Add Recently Used ui
        GameObject recentlyUsedObj = UIFactory.Instance.Create(Type.RECENTLY_USED);
        RecentlyUsedAssets recentlyUsed = recentlyUsedObj.GetComponent<RecentlyUsedAssets>();
        // Add listener to update recently used
        selectedTexture.onImageChange.AddListener(delegate
        {
            recentlyUsed.Add(selectedTexture.data, (AssetData selected) => {
                selectedTexture.data = selected;
                selectedTexture.selectedImage.texture = selected.texture;
                selectedTexture.onImageChange.Invoke();
            });
        });
        uiElements.Add(recentlyUsedObj);

        // Add brush settings category
        GameObject brushCategoryObj = UIFactory.Instance.Create(Type.CATEGORY_TITLE);
        TMP_Text brushCategory = brushCategoryObj.GetComponent<TMP_Text>();
        brushCategory.text = "Brush Settings";
        uiElements.Add(brushCategoryObj);

        // Add brush size slider
        Slider bsSlider = UIFactoryHelper.CreateSlider("Size", 5, 2000, brush.properties["radius"]);
        bsSlider.onValueChanged.AddListener(delegate
        {
            brush.properties["radius"] = bsSlider.value;
            SelectBrushTexture(sourceBrushTexture);
            DrawBrushCursorAtMiddle();
        });
        uiElements.Add(bsSlider.gameObject);

        // Add brush opacity slider
        Slider boSlider = UIFactoryHelper.CreateSlider("Opacity", 0, 1, brush.properties["opacity"]);
        boSlider.onValueChanged.AddListener(delegate
        {
            brush.properties["opacity"] = boSlider.value;
            DrawBrushCursorAtMiddle();
        });
        uiElements.Add(boSlider.gameObject);

        // Add texture size input
        Slider tsSlider = UIFactoryHelper.CreateSlider("Texture Size", 1, 10, 3);
        tsSlider.onValueChanged.AddListener(delegate
        {
            Texture2D resized = PaintTool.Resize(
                selectedTexture.data.texture,
                (int)((float)selectedTexture.data.texture.width * tsSlider.value / 5),
                (int)((float)selectedTexture.data.texture.height * tsSlider.value / 5)
            );
            SelectTexture(resized);
            DrawBrushCursorAtMiddle();
        });
        selectedTexture.onImageChange.AddListener(delegate
        {
            tsSlider.value = 3;
        });
        uiElements.Add(tsSlider.gameObject);
    }
    public void Start()
    {
        // Set material as previewRt
        terrain.GetComponent<Renderer>().material.mainTexture = previewRt;
    }
    public void Stop()
    {
        ResetPreview();
        if (gridEnabled)
            DrawGrid();
    }
    public void Update()
    {
        if(terrain.RaycastTerrain(Input.mousePosition, out RaycastHit hitInfo, false, LayerMask.GetMask("Terrain")))
        {
            float radius = brush.properties["radius"];
            Vector2 uvHit = hitInfo.textureCoord;

            // Point of hit - radius/2 to center the hit of the brush
            int dx = (int)(mainRt.width * uvHit.x)  - (int)radius / 2;
            int dy = (int)(mainRt.height * uvHit.y) - (int)radius / 2;

            if (Input.GetMouseButton(0)) // If click, then paint on texture
            {
                // Continue painting only when mouse has moved
                if (prevMousePos == Input.mousePosition)
                    return;
                else
                    prevMousePos = Input.mousePosition;

                DrawToPreview(dx, dy);
            }
            else if (Input.GetMouseButtonUp(0)) // Mouse lifted, copy new stuff to mainRt
                ApplyPreviewToMain();
            else // Mouse not clicked, show sample
                DrawCursor(dx, dy);

            if(gridEnabled)
                DrawGrid();
        }
    }
    public void PopulateToolPanel(ToolPanel toolPanel)
    {
        foreach (GameObject uiElement in uiElements)
            toolPanel.Add(uiElement);
    }
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // Util functions
    public void SelectTexture(Texture2D selectedTexture)
    {
        paintTool.CopyRepeating(selectedTexture, sourceTexture);
        sourceTexture.Apply();
    }
    public void SelectBrushTexture(Texture2D selectedBrushTexture)
    {
        sourceBrushTexture = PaintTool.Resize(selectedBrushTexture, (int)brush.properties["radius"], (int)brush.properties["radius"]);
    }
}
