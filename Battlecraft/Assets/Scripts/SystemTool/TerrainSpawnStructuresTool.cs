using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SystemToolUI;

public class TerrainSpawnStructuresTool : SystemTool
{
    // List of UI elements
    private List<GameObject> uiElements = new List<GameObject>();
    // Reference to terrain
    private TerrainMesh terrain;
    // Brush for Multispawn
    private Brush brush;
    // Previous mouse position
    private Vector3 prevMousePos;

    // Map of structures spawned and their tiles
    Dictionary<Tile, AssetData> structureMap = new Dictionary<Tile, AssetData>();
    // Custom grid for structure placement
    bool[,] placementMatrix;


    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    private List<AssetData> selectedCollection = new List<AssetData>();
    private GameObject selectedObject
    {
        get
        {
            return selectedCollection[0].gameObject;
        }
    }
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // spawnBuffer methods
    AssetData pendingAsset = null;
    public AssetData ApplySpawn()
    {
        var permanentAsset = pendingAsset;
        pendingAsset = null;
        return permanentAsset;
    }
    public AssetData ApplySpawn(List<Tile> tilesOfInterest)
    {
        AssetData data = ApplySpawn();

        foreach (Tile tile in tilesOfInterest)
            structureMap.Add(tile, data);

        return data;
    }
    public void ClearSpawn()
    {
        MonoBehaviour.Destroy(pendingAsset.self);
        pendingAsset = null;
    }
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // Interface functions
    public void Initialize(TerrainMesh terrain)
    {
        // Set terrain reference
        this.terrain = terrain;

        // Initialize Brush
        brush = new Brush();
        brush.properties.Add("objectSize", 100);
        brush.properties.Add("density", 75);
        brush.properties.Add("randomTransform", 20f);

        AssetLoader loader = new AssetLoader("objects");
        selectedCollection.Add(loader.assetsData[0]);

        // Init placement matrix. Matrix size always corresponds with GridManager.Instance.grid
        // AKA, one placementMatrix cell for each tile.
        Grid grid = GridManager.Instance.grid;
        placementMatrix = new bool[grid.sizeX, grid.sizeZ];
        for(int i = 0; i < grid.sizeX; i++)
        {
            for(int j = 0; j < grid.sizeZ; j++)
            {
                placementMatrix[i, j] = false;
            }
        }
    }
    public void GenerateUI()
    {
        // Create UI for selected object
        GameObject selectedObjectCollectionObj = UIFactory.Instance.Create(Type.SELECTED_OBJECT_COLLECTION);
        SelectedObjectCollection selectedObjectCollection = selectedObjectCollectionObj.GetComponent<SelectedObjectCollection>();
        selectedObjectCollection.filteredTags = new List<string>();
        selectedObjectCollection.filteredTags.Add("structure");
        selectedObjectCollection.onSelectedChange.AddListener(delegate
        {
            selectedCollection = selectedObjectCollection.selectedCollection;
            ClearSpawn();
        });
        uiElements.Add(selectedObjectCollectionObj);

        // Create UI for recently used asset
        GameObject recentlyUsedObj = UIFactory.Instance.Create(Type.RECENTLY_USED);
        RecentlyUsedAssets recenltyUsed = recentlyUsedObj.GetComponent<RecentlyUsedAssets>();
        selectedObjectCollection.onSelectedChange.AddListener(delegate
        {
            var selectedData = selectedObject.GetComponent<AssetData>();
            recenltyUsed.Add(selectedData, (AssetData data) =>
            {
                ClearSpawn();
                selectedCollection.Clear();
                selectedCollection.Add(data);
                selectedObjectCollection.mainImage.texture = data.texture;
            });
        });
        uiElements.Add(recentlyUsedObj);
    }
    public void Start()
    {
        brush.CreateCursor();
    }
    public void Stop()
    {
        ClearSpawn();
        brush.DeleteCursor();
    }
    public void Update()
    {
        if (terrain.RaycastTerrain(Input.mousePosition, out RaycastHit hitInfo, false, LayerMask.GetMask("Terrain")))
        {
            // Return if mouse hasnt moved
            if (prevMousePos == Input.mousePosition)
                return;
            // Check if pending exists or spawn
            if (pendingAsset == null)
                pendingAsset = MonoBehaviour.Instantiate(selectedObject, new Vector3(0f, 0f, 0f), new Quaternion()).GetComponent<AssetData>();

            AssetData data = pendingAsset;

            // Get affected tiles based on collider
            BoxCollider collider = data.self.GetComponent<BoxCollider>();
            Bounds bounds = collider.bounds;
            List<Tile> tiles = GetTilesOfInterest(new Rect(
                new Vector2(bounds.min.x, bounds.min.z),
                new Vector2(bounds.max.x, bounds.max.z)
            ));

            // Input Handling
            if (Input.GetMouseButtonDown(0))
            {
                if (CanBePlaced(tiles))
                {
                    ApplySpawn(tiles);
                    prevMousePos = Input.mousePosition;

                    // Mark placement Matrix tiles
                    SetPlacementMatrix(tiles, true);
                    // Flatten terrain
                    FlattenTerrain(tiles, data.transform.position.y);
                }
                else
                {
                    Debug.Log("Cant be placed!");
                    // Indicate why it cant be placed
                    //List<Tile> overlapTiles = GetOverlapingTiles(tiles)
                }
            }
            else
            {
                // Check Grid placement
                Grid grid = GridManager.Instance.grid;
                Tile tile = grid.GetTile(hitInfo.point);

                // Get near structure if it exists and potentially change target height
                float targetHeight = hitInfo.point.y;
                AssetData adjacentStructure = GetAdjacentStructure(tiles);
                if (adjacentStructure != null)
                    targetHeight = adjacentStructure.transform.position.y;

                // Place on grid locked position
                pendingAsset.self.transform.position = new Vector3(tile.x, targetHeight, tile.z);
                GameObjectManager.Instance.UpdateObject(pendingAsset.self);
            }
        }
    }
    public void PopulateToolPanel(ToolPanel toolPanel)
    {
        foreach (GameObject uiElement in uiElements)
            toolPanel.Add(uiElement);
    }
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // Functions to flatten and smooth the terrain around a placed object.
    private List<Tile>  GetTilesOfInterest(Rect rect)
    {
        return GridManager.Instance.grid.GetTilesInRect(rect);
    }
    private List<Tile>  GetAdjacentTiles(List<Tile> tiles) // Tiles that are direct neighbors of tiles.
    {
        List<Tile> adjacentTiles = new List<Tile>();

        Grid grid = GridManager.Instance.grid;
        foreach(Tile tile in tiles)
        {
            List<Tile> neighbors = grid.GetTileNeighbors(tile);
            foreach (Tile neighbor in neighbors)
                if (tiles.Contains(neighbor) == false && adjacentTiles.Contains(neighbor) == false)
                    adjacentTiles.Add(neighbor);
        }

        return adjacentTiles;
    }
    private void        FlattenTerrain(List<Tile> tiles, float height)
    {
        // Tileset for performance checks
        HashSet<Tile> tileSet = new HashSet<Tile>(tiles);

        // Get references for vertices and grid
        Vector3[] vertices = terrain.GetVerticesRef();
        Grid grid = GridManager.Instance.grid;

        // Structures for inner bfs expansion
        Stack<int> innerBfsStack = new Stack<int>();
        HashSet<int> bfsChecked = new HashSet<int>();

        // Structure for outer bfs expansion
        Stack<int> outerBfsStack = new Stack<int>();

        // Inner bfs expansideeeon. Flatten the vertices that belong to tiles, 
        foreach(Tile tile in tileSet)
        {
            innerBfsStack.Push(tile.vertices[Random.Range(0, tile.vertices.Count - 1)]);
            break;
        }

        while(innerBfsStack.Count > 0)
        {
            int index = innerBfsStack.Pop();
            Vector3 pos = vertices[index];

            if (bfsChecked.Contains(index))
                continue;

            // Apply bfs function
            vertices[index].y = height;
            bfsChecked.Add(index);

            // Push neighbors
            List<int> neighborIndices = terrain.GetVertexNeighbors(index);
            foreach(int neighborIndex in neighborIndices)
            {
                Vector3 neighborPos = vertices[neighborIndex];
                Tile neighborTile = grid.GetTile(neighborPos);
                if (tileSet.Contains(neighborTile))
                {
                    innerBfsStack.Push(neighborIndex);
                }
                else // Edge vertex, add for outerbfs
                {
                    outerBfsStack.Push(neighborIndex);
                }
            }
        }

        // Get tiles of outer bfs stacks in a tileset
        HashSet<Tile> outerTileset = new HashSet<Tile>(GetAdjacentTiles(tiles));

        // Start outer bfs by list layers
        int steps = 10;
        float reduceStep = 1f / (float)steps;
        float p = 1f - reduceStep;
        for(int i = 0; i < steps; i++)
        {
            List<int> step = new List<int>(outerBfsStack);
            outerBfsStack.Clear();

            foreach(int index in step)
            {
                // Continue if index has already been checked or if it does not belong to our outer tileset
                if (bfsChecked.Contains(index) /*|| outerTileset.Contains(grid.GetTile(vertices[index])) == false*/)
                    continue;
                bfsChecked.Add(index);

                // else change height and add to bfs checked
                vertices[index].y = (1f - p) * vertices[index].y + p * height;

                // Get neighbors
                foreach (int neighborIndex in terrain.GetVertexNeighbors(index))
                    outerBfsStack.Push(neighborIndex);

                terrain.gizmoStack1.Add(vertices[index]);
            }
            
            p -= reduceStep;
        }

        terrain.UpdateMesh();
        GameObjectManager.Instance.UpdateObjectsHeightInTiles(terrain, tiles);
    }
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // Functions to control placement matrix
    private void        SetPlacementMatrix(List<Tile> tiles, bool value)
    {
        Grid grid = GridManager.Instance.grid;
        foreach(Tile tile in tiles)
        {
            int i = Mathf.FloorToInt(tile.x / grid.tileUnit);
            int j = Mathf.FloorToInt(tile.z / grid.tileUnit);
            placementMatrix[i, j] = value;
        }
    }
    private bool        CanBePlaced(List<Tile> tiles)
    {
        Grid grid = GridManager.Instance.grid;
        foreach (Tile tile in tiles)
        {
            int i = Mathf.FloorToInt(tile.x / grid.tileUnit);
            int j = Mathf.FloorToInt(tile.z / grid.tileUnit);
            if (placementMatrix[i, j] == true)
                return false;
        }

        return true;
    }
    private List<Tile>  GetOverlapingTiles(List<Tile> tiles1, List<Tile> tiles2)
    {
        return tiles1;
    }
    private AssetData   GetAdjacentStructure(List<Tile> tiles)
    {
        Grid grid = GridManager.Instance.grid;
        foreach(Tile tile in tiles)
        {
            List<Tile> neighbors = grid.GetTileNeighbors(tile);
            foreach (Tile neighbor in neighbors)
                if (structureMap.ContainsKey(neighbor))
                    return structureMap[neighbor];
        }
        return null;
    }
}
