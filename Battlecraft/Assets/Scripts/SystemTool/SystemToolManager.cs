using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SystemToolType
{
    SELECT,
    SCULPT,
    PAINT,
    SPAWN_ENV,
    SPAWN_STRUCTURE,
    WEATHER,
    RENDER
}

// Singleton Class
public class SystemToolManager : MonoBehaviour
{

    // Active Current Tool
    SystemTool tool;
    public SystemToolType type = SystemToolType.SCULPT;

    // System tools dispatcher
    private Dictionary<SystemToolType, SystemTool> systemTools = new Dictionary<SystemToolType, SystemTool>();

    // Scene Refs
    public TerrainMesh terrain;
    public ToolPanel toolPanel;

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    private void Start()
    {
        Application.targetFrameRate = 60;

        systemTools.Add(SystemToolType.SELECT, new TerrainSelectTool());
        systemTools.Add(SystemToolType.SCULPT, new TerrainSculptTool());
        systemTools.Add(SystemToolType.PAINT, new TerrainPaintTool());
        systemTools.Add(SystemToolType.SPAWN_ENV, new TerrainSpawnEnvironmentTool());
        systemTools.Add(SystemToolType.SPAWN_STRUCTURE, new TerrainSpawnStructuresTool());
        systemTools.Add(SystemToolType.WEATHER, new TerrainWeatherTool());

        // Initialize all system tools
        foreach (KeyValuePair<SystemToolType, SystemTool> pair in systemTools)
        {
            pair.Value.Initialize(terrain);
            pair.Value.GenerateUI();
        }

        ChangeTool(SystemToolType.SCULPT);
    }
    private void Update()

    {
        if (tool != null)
            tool.Update();
    }
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    public void         ChangeTool(SystemToolType type)
    {
        if (tool != null)
            tool.Stop();

        toolPanel.Clear();
        tool = systemTools[type];
        tool.Start();
        tool.PopulateToolPanel(toolPanel);
    }
    public SystemTool   GetSystemTool(SystemToolType type)
    {
        return systemTools[type];
    }
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // Singleton Properties
    public static SystemToolManager Instance { get; private set; }
    private void Awake()
    {
        // If there is an instance, and it's not me, delete myself.

        if (Instance != null && Instance != this)
        {
            Destroy(this);
        }
        else
        {
            Instance = this;
        }
    }
    private void OnApplicationQuit()
    {
        AssetLoader.UnloadAll();
    }
}
