using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SystemToolUI;

public class TerrainSpawnEnvironmentTool : SystemTool
{
    public enum SpawnMode
    {
        SINGLE,
        POLY,
        ERASE
    }
    private SpawnMode mode = SpawnMode.SINGLE;

    // List of UI elements
    private List<GameObject> uiElements = new List<GameObject>();
    // Reference to terrain
    private TerrainMesh terrain;
    // Brush for Multispawn
    private Brush brush;
    // Previous mouse position
    private Vector3 prevMousePos;

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    private List<AssetData> selectedCollection = new List<AssetData>();
    private GameObject      selectedObject
    {
        get
        {
            return selectedCollection[0].gameObject;
        }
    }
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // spawnBuffer methods
    Stack<AssetData> spawnBuffer = new Stack<AssetData>();
    public void ApplySpawnBuffer()
    {
        spawnBuffer.Clear();
    }
    public void ClearSpawnBuffer()
    {
        while (spawnBuffer.Count > 0)
            MonoBehaviour.Destroy(spawnBuffer.Pop().self);
    }
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // Main methods of SpawnTool
    public void DrawCursor_Single(Vector3 point)
    {
        ObjectGroup group = GameObjectManager.Instance.GetGroup(selectedObject.GetComponent<AssetData>().mainTag);
        if(spawnBuffer.Count == 0)
        {
            group.ChangeDensityValue(point, true);
            GameObject obj = SingleSpawn(point);
            spawnBuffer.Push(obj.GetComponent<AssetData>());
        }

        AssetData pendingAsset = spawnBuffer.Pop();
        pendingAsset.self.transform.position = point;
        GameObjectManager.Instance.UpdateObject(pendingAsset.self);
        spawnBuffer.Push(pendingAsset);
    }
    public void DrawCursor_Poly(Vector3 point)
    {
        ObjectGroup group = GameObjectManager.Instance.GetGroup(selectedObject.GetComponent<AssetData>().mainTag);

        brush.DrawCursor(point);

        for (int i = 0; i < 100 - spawnBuffer.Count; i++)
        {
            // Get a position
            Vector3 position = point + GetRandomPointInRadius(brush.properties["radius"]);

            if (group.CanBeAdded(position, brush.properties["density"]))
            {
                group.ChangeDensityValue(position, true);
                GameObject obj = SingleSpawn(position);
                spawnBuffer.Push(obj.GetComponent<AssetData>());
            }
        }

        Stack<AssetData> spawnBuffer2 = new Stack<AssetData>();
        while (spawnBuffer.Count > 0)
        {
            // Pop from stack
            AssetData pendingAsset = spawnBuffer.Pop();
            spawnBuffer2.Push(pendingAsset);

            if (Vector3.Distance(pendingAsset.transform.position, point) > brush.properties["radius"])
            {
                spawnBuffer2.Pop();
                MonoBehaviour.Destroy(pendingAsset.gameObject);
            }
        }
        while (spawnBuffer2.Count > 0)
            spawnBuffer.Push(spawnBuffer2.Pop());
    }
    public void DrawCursor_Erase(Vector3 point)
    {

    }
    public void Update_Single(Vector3 point)
    {
        // Return if mouse hasnt moved
        if (prevMousePos == Input.mousePosition)
            return;

        // Input Handling
        if (Input.GetMouseButtonDown(0))
        {
            ApplySpawnBuffer();
            prevMousePos = Input.mousePosition;
        }
        else
            DrawCursor_Single(point);
    }
    public void Update_Poly(Vector3 point)
    {
        // Return if mouse hasnt moved
        if (prevMousePos == Input.mousePosition)
            return;

        // Input Handling
        if (Input.GetMouseButton(0))
        {
            ApplySpawnBuffer();
            prevMousePos = Input.mousePosition;
        }
        DrawCursor_Poly(point);
        brush.DrawCursor(point);
    }
    public void Update_Erase(Vector3 point)
    {
        if (prevMousePos == Input.mousePosition)
            return;

        if (Input.GetMouseButton(0))
        {
            Grid grid = GridManager.Instance.grid;

            List<Tile> tiles = grid.GetTilesInRadius(point, brush.properties["radius"]);
            foreach(Tile tile in tiles)
            {
                List<GameObject> objects = tile.objects;
                for(int i = objects.Count - 1; i >= 0; i--)
                {
                    GameObject obj = objects[i];
                    if (Vector3.Distance(obj.transform.position, point) < brush.properties["radius"])
                        MonoBehaviour.Destroy(obj);
                }
            }
        }

        brush.DrawCursor(point);
    }
    public GameObject SingleSpawn(Vector3 position)
    {
        // Try add at object group or delete and try again
        GameObject instance = MonoBehaviour.Instantiate(selectedObject);
        // Find closest vertex to define y height of instance
        Grid grid = GridManager.Instance.grid;
        Tile tile = grid.GetTile(position);
        Vector3 closestVertex = terrain.FindClosestVertex(position);
        // Set position
        instance.transform.position = new Vector3(position.x, closestVertex.y, position.z);
        GameObjectManager.Instance.UpdateObject(instance);
        // Get random factor
        float baseScale = brush.properties["objectSize"];
        float randomTransform = brush.properties["randomTransform"];
        // Randomize scale/rotation
        instance.transform.localScale *= baseScale;
        instance.transform.localScale += new Vector3(
            Random.Range(0f, randomTransform * 0.01f),
            Random.Range(0f, randomTransform * 0.01f),
            Random.Range(0f, randomTransform * 0.01f)
        );
        instance.transform.eulerAngles += new Vector3(
            Random.Range(0f, randomTransform * 0.15f),
            Random.Range(0f, randomTransform * 0.70f),
            Random.Range(0f, randomTransform * 0.15f)
        );
        return instance;
    }
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // Interface functions
    public void Initialize(TerrainMesh terrain)
    {
        // Set terrain reference
        this.terrain = terrain;

        mode = SpawnMode.POLY;

        // Initialize Brush
        brush = new Brush();
        brush.properties.Add("objectSize", 1);
        brush.properties.Add("density", 75);
        brush.properties.Add("randomTransform", 20f);

        GameObjectManager.Instance.InitGroup("undef", 0.1f);
        GameObjectManager.Instance.InitGroup("structure", 3f);
        GameObjectManager.Instance.InitGroup("tree", 6f);
        GameObjectManager.Instance.InitGroup("grass", 0.1f);
        GameObjectManager.Instance.InitGroup("rock", 6f);

        AssetLoader loader = new AssetLoader("objects");
        selectedCollection.Add(loader.assetsData[0]);
    }
    public void GenerateUI()
    {
        // Mode of spawning
        GameObject radialButtonsObj = UIFactory.Instance.Create(Type.RADIAL_BUTTONS);
        RadialButtons radialButtons = radialButtonsObj.GetComponent<RadialButtons>();
        RadialButton b1 = radialButtons.AddButton();    // Single Spawn
        b1.onButtonClick.AddListener(delegate
        {
            mode = SpawnMode.ERASE;
            ClearSpawnBuffer();
        });
        RadialButton b2 = radialButtons.AddButton();    // Polyspawn
        b2.onButtonClick.AddListener(delegate
        {
            mode = SpawnMode.POLY;
            ClearSpawnBuffer();
        });
        RadialButton b3 = radialButtons.AddButton();    // Eraser
        b3.onButtonClick.AddListener(delegate
        {
            mode = SpawnMode.SINGLE;
            ClearSpawnBuffer();
        });
        uiElements.Add(radialButtonsObj);

        // Create UI for selected object
        GameObject selectedObjectCollectionObj = UIFactory.Instance.Create(Type.SELECTED_OBJECT_COLLECTION);
        SelectedObjectCollection selectedObjectCollection = selectedObjectCollectionObj.GetComponent<SelectedObjectCollection>();
        selectedObjectCollection.filteredTags = null;
        selectedObjectCollection.onSelectedChange.AddListener(delegate
        {
            selectedCollection = selectedObjectCollection.selectedCollection;
            ClearSpawnBuffer();
        });
        uiElements.Add(selectedObjectCollectionObj);

        // Create UI for recently used asset
        GameObject recentlyUsedObj = UIFactory.Instance.Create(Type.RECENTLY_USED);
        RecentlyUsedAssets recenltyUsed = recentlyUsedObj.GetComponent<RecentlyUsedAssets>();
        selectedObjectCollection.onSelectedChange.AddListener(delegate
        {
            var selectedData = selectedObject.GetComponent<AssetData>();
            recenltyUsed.Add(selectedData, (AssetData data) =>
            {
                ClearSpawnBuffer();
                selectedCollection.Clear();
                selectedCollection.Add(data);
                selectedObjectCollection.mainImage.texture = data.texture;
            });
        });
        uiElements.Add(recentlyUsedObj);

        // Add Slider for radius
        Slider slider = UIFactoryHelper.CreateSlider("Brush Size", 0, 100, brush.properties["radius"]);
        slider.onValueChanged.AddListener(delegate {
            brush.properties["radius"] = slider.value;
            slider.valueLabel.text = slider.value.ToString("F1");
        });
        uiElements.Add(slider.gameObject);

        // Add Slider for size
        Slider slider2 = UIFactoryHelper.CreateSlider("Object Size", 0.1f, 4, brush.properties["objectSize"]);
        slider2.format = "F2";
        slider2.onValueChanged.AddListener(delegate {
            brush.properties["objectSize"] = slider2.value;
            slider2.valueLabel.text = slider2.value.ToString("F1");
            ClearSpawnBuffer();
        });
        b3.onButtonClick.AddListener(delegate
        {
            slider2.gameObject.SetActive(false);
        });
        b2.onButtonClick.AddListener(delegate
        {
            slider2.gameObject.SetActive(true);
        });
        uiElements.Add(slider2.gameObject);

        // Add slider for density
        Slider slider3 = UIFactoryHelper.CreateSlider("Density", 0, 100, brush.properties["density"]);
        slider3.onValueChanged.AddListener(delegate {
            brush.properties["density"] = slider3.value;
            slider3.valueLabel.text = slider3.value.ToString("F1");
        });
        b1.onButtonClick.AddListener(delegate
        {
            slider3.gameObject.SetActive(false);
        });
        b3.onButtonClick.AddListener(delegate
        {
            slider3.gameObject.SetActive(true);
        });
        uiElements.Add(slider3.gameObject);

        // Add slider for random transform
        Slider slider4 = UIFactoryHelper.CreateSlider("Random Transform", 0, 100, brush.properties["randomTransform"]);
        slider4.onValueChanged.AddListener(delegate {
            brush.properties["randomTransform"] = slider4.value;
            slider4.valueLabel.text = slider4.value.ToString("F1");
            ClearSpawnBuffer();
        });
        uiElements.Add(slider4.gameObject);
    }
    public void Start()
    {
        brush.CreateCursor();
    }
    public void Stop()
    {
        ClearSpawnBuffer();
        brush.DeleteCursor();
        Debug.Log("Deleted cursor");
    }
    public void Update()
    {
        if (terrain.RaycastTerrain(Input.mousePosition, out RaycastHit hitInfo, false, LayerMask.GetMask("Terrain")))
        {
            if (mode == SpawnMode.SINGLE)
                Update_Single(hitInfo.point);
            else if (mode == SpawnMode.POLY)
                Update_Poly(hitInfo.point);
            else if (mode == SpawnMode.ERASE)
                Update_Erase(hitInfo.point);
        }
    }
    public void PopulateToolPanel(ToolPanel toolPanel)
    {
        foreach (GameObject uiElement in uiElements)
            toolPanel.Add(uiElement);
    }
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // Util functions
    private Vector3     GetRandomPointInRadius(float radius)
    {
        // Calculate random point in the radius of the 
        float r = radius * Mathf.Sqrt(Random.Range(0f, 1f));
        float theta = Random.Range(0f, 1f) * 2 * Mathf.PI;

        Vector3 randomPos = new Vector3(
            r * Mathf.Cos(theta),
            0,
            r * Mathf.Sin(theta)
        );

        return randomPos;
    }
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
}