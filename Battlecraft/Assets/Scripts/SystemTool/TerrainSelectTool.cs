using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SystemToolUI;
using UnityEngine.EventSystems;

public class TerrainSelectTool : SystemTool
{
    // List of UI elements
    private List<GameObject> uiElements = new List<GameObject>();
    // Reference to terrain
    private TerrainMesh terrain;
    // Brush for Multispawn
    private Brush brush;
    // Previous mouse position
    private Vector3 prevMousePos;

    // Gizmo instance
    private GizmoHandle gizmoHandle;

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // Interface functions
    public void Initialize(TerrainMesh terrain)
    {
        GameObjectManager.Instance.InitGroup("gizmo", 0.1f);

        // Set terrain reference
        this.terrain = terrain;

        // Initialize Brush
        brush = new Brush();

        // Load gizmo prefab, spawn it and get reference of gizmo
        //AssetLoader loader = new AssetLoader("gizmos");
        GameObject gizmoPrefab = Resources.Load<GameObject>("Gizmo/GizmoHandle");
        gizmoHandle = MonoBehaviour.Instantiate(gizmoPrefab).GetComponent<GizmoHandle>();
        gizmoHandle.gameObject.SetActive(false);
    }
    public void GenerateUI()
    {
        // Create Translate category
        uiElements.Add(UIFactoryHelper.CreateCategory("Translate"));
        VectorInputField translateVector = UIFactoryHelper.CreateVectorInputField("X", "Y", "Z", 1f, "", "F2");
        gizmoHandle.onSelectionChange.AddListener(delegate
        {
            translateVector.value = gizmoHandle.transform.position;
        });
        gizmoHandle.onTransformChange.AddListener(delegate
        {
            translateVector.value = gizmoHandle.transform.position;
        });
        translateVector.onValueChanged.AddListener(delegate
        {
            gizmoHandle.transform.position = translateVector.value;
        });
        uiElements.Add(translateVector.gameObject);

        // Add other settings category
        uiElements.Add(UIFactoryHelper.CreateCategory("Other Settings"));
        Checkbox isLockedOnTerrain = UIFactoryHelper.CreateCheckbox("Is Locked On Terrain", true);
        isLockedOnTerrain.onToggleChange.AddListener((bool value) => {
            gizmoHandle.selected.isLockedOnTerrain = value;
        }); 
        uiElements.Add(isLockedOnTerrain.gameObject);
    }
    public void Start()
    {
        // Enable all prefab colliders
        var objects = GameObjectManager.Instance.GetObjects();
        foreach(GameObject obj in objects)
        {
            var collider = obj.GetComponent<Collider>();
            if (collider)
                collider.enabled = true;
        }
        brush.CreateCursor();


        gizmoHandle.gameObject.SetActive(true);
    }
    public void Stop()
    {
        // Disable all prefab colliders
        var objects = GameObjectManager.Instance.GetObjects();
        foreach (GameObject obj in objects)
        {
            var collider = obj.GetComponent<Collider>();
            if (collider)
                collider.enabled = false;
        }
        brush.DeleteCursor();
        // Destroy gizmo
        gizmoHandle.gameObject.SetActive(false);
    }
    public void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out RaycastHit hitInfo) && !EventSystem.current.IsPointerOverGameObject())
            {
                AssetData assetData = hitInfo.transform.gameObject.GetComponent<AssetData>();
                if (assetData != null)
                {
                    gizmoHandle.SetSelected(assetData);
                }
            }
        }
    }
    public void PopulateToolPanel(ToolPanel toolPanel)
    {
        foreach (GameObject uiElement in uiElements)
            toolPanel.Add(uiElement);
    }
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // Util functions
    
}
