using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameObjectManager : MonoBehaviour
{
    private List<GameObject> objects = new List<GameObject>();
    private Dictionary<string, ObjectGroup> objectGroups = new Dictionary<string, ObjectGroup>();

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    public void Add(GameObject obj, string tag = "undef")
    {
        // Add to GO Manager lists
        objects.Add(obj);
        if (objectGroups.ContainsKey(tag) == false)
            objectGroups.Add(tag, new ObjectGroup());
        objectGroups[tag].Add(obj);
        // Add to grid
        Grid grid = GridManager.Instance.grid;
        grid.Add(obj);
    }
    public void Remove(GameObject obj, string tag = "undef")
    {
        objects.Remove(obj);
        objectGroups[tag].Remove(obj);
        GridManager.Instance.grid.Remove(obj);
    }
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    public List<GameObject>                 GetObjects()
    {
        return objects;
    }
    public ObjectGroup                      GetGroup(string tag)
    {
        if(objectGroups.ContainsKey(tag) == false)
            objectGroups.Add(tag, new ObjectGroup());
        return objectGroups[tag];
    }
    public Dictionary<string, ObjectGroup>  GetGroups()
    {
        return objectGroups;
    }
    public void                             InitGroup(string tag, float tileSize)
    {
        objectGroups.Add(tag, new ObjectGroup());

        if(tileSize > 0)
            objectGroups[tag].InitDensityMatrix(tileSize);
        // Else object is a structure
    }
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    public void UpdateObject(GameObject obj)
    {
        Grid grid = GridManager.Instance.grid;
        grid.Add(obj);
    }
    public void UpdateObjectsHeightInTiles(TerrainMesh terrain, List<Tile> tiles)
    {
        foreach(Tile tile in tiles)
        {
            foreach(GameObject obj in tile.objects)
            {
                AssetData data = obj.GetComponent<AssetData>();
                if (data.isLockedOnTerrain == false)
                    continue;

                Vector3 closest = terrain.FindClosestVertex(obj.transform.position);
                obj.transform.position = new Vector3(obj.transform.position.x, closest.y, obj.transform.position.z);
            }
        }
    }
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // Singleton Properties
    public static GameObjectManager Instance { get; private set; }
    private void Awake()
    {
        // If there is an instance, and it's not me, delete myself.

        if (Instance != null && Instance != this)
        {
            Destroy(this);
        }
        else
        {
            Instance = this;
        }
    }
}
