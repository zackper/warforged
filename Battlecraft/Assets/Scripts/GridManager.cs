using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile
{
    public float x;
    public float z;
    public Vector3 position
    {
        get
        {
            return new Vector3(x, 0, z);
        }
        set
        {
            x = value.x;
            z = value.z;
        }
    }

    public List<GameObject>     objects;
    public List<int>            vertices;

    public Tile(float x, float z)
    {
        position = new Vector3(x, 0, z);
        objects = new List<GameObject>();
        vertices = new List<int>();
    }
}

// Keep in mind, that indexing is now done by intervals of one meter.
// To change the tile size, simpy do % tileSize (meters).
public class Grid
{
    // 1.524 -> 5ft. So each tile should me 5ft square.
    public float    tileUnit = 1.524f; // Tile unit is in meters == default unity unit
    public int      sizeX, sizeZ;
    private Tile[,] tiles;
    // Dictionaries for faster accesses for object translations
    private Dictionary<GameObject, Tile> objectTiles = new Dictionary<GameObject, Tile>();

    /// <summary>
    /// Size X and Z are given in meters. Grid will translate it to its local unit of measurement using tileUnit.
    /// </summary>
    /// <param name="sizeX"></param>
    /// <param name="sizeZ"></param>
    public Grid(int sizeX, int sizeZ)
    {
        this.sizeX = (int)Mathf.Ceil(sizeX / tileUnit);
        this.sizeZ = (int)Mathf.Ceil(sizeZ / tileUnit);

        tiles = new Tile[this.sizeX, this.sizeZ];
        for(int x = 0; x < this.sizeX; x++)
        {
            for(int z = 0; z < this.sizeZ; z++)
            {
                tiles[x, z] = new Tile((float)x * tileUnit, (float)z * tileUnit);
            }
        }
    }

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    public void Add(GameObject obj)
    {
        Tile tile = GetTile(obj);

        // If new object tile is different than old one
        if (objectTiles.ContainsKey(obj))
        {
            if (objectTiles[obj] != tile)
            {
                // Remove object from previous tile object list
                objectTiles[obj].objects.Remove(obj);
                // Assign new tile
                objectTiles[obj] = tile;
                // Add it to new tile object list
                tile.objects.Add(obj);
            }
        }
        // First time adding object to grid
        else
        {
            tile.objects.Add(obj);
            objectTiles.Add(obj, tile);
        }
    }
    public void Remove(GameObject obj)
    {
        Tile tile = GetTile(obj);
        tile.objects.Remove(obj);
        objectTiles.Remove(obj);
    }
    public void Add(float x, float z, int vertexIndex)
    {
        // Vertices never move on the x, z axis so no reason 
        // to have dictionary for them. Their position is for ever.
        Tile tile = GetTile(x, z);
        tile.vertices.Add(vertexIndex);
    }
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    public Tile     GetTile(float x, float z)
    {
        return tiles[(int)(x / tileUnit), (int)(z / tileUnit)];
    }
    public Tile     GetTile(Vector3 position)
    {
        return GetTile(position.x, position.z);
    }
    public Tile     GetTile(GameObject obj)
    {
        return GetTile(obj.transform.position);
    }
    public Tile[,]  GetTiles()
    {
        return tiles;
    }
    public float    GetTileUnit()
    {
        return tileUnit;
    }
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    public List<Tile>   GetTileNeighbors(Tile tile)
    {
        Vector3Int matrixPos = Vector3Int.FloorToInt(tile.position / tileUnit);
        
        List<Tile> neighbors = new List<Tile>();
        // I literaly cant understand why m, n must be bound to 2 instead of 1.
        // If I use <= 1, then at some particular size it does not return neighbors correctly.
        for (int m = -1; m <= 2; m++)
        {
            for (int n = -1; n <= 2; n++)
            {
                if (
                    matrixPos.x + m >= 0 && matrixPos.x + m < sizeX &&
                    matrixPos.z + n >= 0 && matrixPos.z + n < sizeZ
                )
                {
                    neighbors.Add(tiles[matrixPos.x + m, matrixPos.z + n]);
                }
            }
        }

        return neighbors;
    }
    public Tile         GetTileNeighbor(Tile tile, int m, int n)
    {
        Vector3Int matrixPos = Vector3Int.FloorToInt(tile.position / tileUnit);

        if (
                    matrixPos.x + m >= 0 && matrixPos.x + m < sizeX &&
                    matrixPos.z + n >= 0 && matrixPos.z + n < sizeZ
            )
        {
            return tiles[matrixPos.x + m, matrixPos.z + n];
        }
        else
            return null;
    }
    public List<Tile>   GetTilesInRect(Rect rect)
    {
        List<Tile> tiles = new List<Tile>();

        // Rect is in global space
        for (float i = rect.x; i < rect.width; i += tileUnit)
        {
            for (float j = rect.y; j < rect.height; j += tileUnit)
            {
                if (
                    0 <= i && i <= sizeX &&
                    0 <= j && j <= sizeZ
                )
                {
                    Tile tile = GetTile(i, j);
                    tiles.Add(tile);
                }
            }
        }

        return tiles;
    }
    public List<Tile>   GetTilesInRadius(Vector3 point, float radius)
    {
        List<Tile> tilesInRange = new List<Tile>();
        Queue<Tile> bfsQueue = new Queue<Tile>();
        Dictionary<Tile, bool> checkedTiles = new Dictionary<Tile, bool>();

        Tile centerTile = GetTile(point);
        bfsQueue.Enqueue(centerTile);

        // For each bfs expansion/iteration
        while (bfsQueue.Count > 0)
        {
            Tile tile = bfsQueue.Dequeue();

            if (checkedTiles.ContainsKey(tile))
                continue;
            else
                checkedTiles.Add(tile, true);

            // Check each corner of tile, if it is within radius
            if (
                Vector3.Distance(point, tile.position) < radius ||
                Vector3.Distance(point, tile.position + new Vector3(tileUnit, 0, 0)) < radius ||
                Vector3.Distance(point, tile.position + new Vector3(0, 0, tileUnit)) < radius ||
                Vector3.Distance(point, tile.position + new Vector3(tileUnit, 0, tileUnit)) < radius
            )
            {
                tilesInRange.Add(tile);
                // Also add neighbors to queue
                foreach (Tile neighbor in GetTileNeighbors(tile))
                    bfsQueue.Enqueue(neighbor);
            }
        }

        return tilesInRange;
    }
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // Debug helpers
    public void         ClearVertices()
    {
        foreach (Tile tile in tiles)
        {
            tile.vertices.Clear();
        }
    }
    public IEnumerator  BFSExpand(Vector3 point, float radius)
    {
        Queue<Tile> bfsQueue = new Queue<Tile>();
        Dictionary<Tile, bool> checkedTiles = new Dictionary<Tile, bool>();

        Tile centerTile = GetTile(point);
        bfsQueue.Enqueue(centerTile);

        // For each bfs expansion/iteration
        while (bfsQueue.Count > 0)
        {
            Tile tile = bfsQueue.Dequeue();
            if (checkedTiles.ContainsKey(tile))
                continue;
            else
                checkedTiles.Add(tile, true);

            GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Capsule);
            cube.transform.localScale = new Vector3(1, 1, 1) * tileUnit;
            cube.transform.position = tile.position;
            yield return null;

            // Check each corner of tile, if it is within radius
            if (
                Vector3.Distance(point, tile.position) < radius ||
                Vector3.Distance(point, tile.position + new Vector3(tileUnit, 0, 0)) < radius ||
                Vector3.Distance(point, tile.position + new Vector3(0, 0, tileUnit)) < radius ||
                Vector3.Distance(point, tile.position + new Vector3(tileUnit, 0, tileUnit)) < radius
            ) 
            {
                // Also add neighbors to check if they are in range as well
                foreach (Tile neighbor in GetTileNeighbors(tile))
                    bfsQueue.Enqueue(neighbor);
            }
        }
    }
    public IEnumerator  ShowAllVertices(TerrainMesh terrain)
    {
        Vector3[] vertices = terrain.GetVerticesRef();
        for(int i = 0; i < sizeX; i++)
        {
            for(int j = 0; j < sizeZ; j++)
            {
                Tile tile = tiles[i, j];
                GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Capsule);
                cube.GetComponent<CapsuleCollider>().enabled = false;
                cube.transform.localScale = new Vector3(1, 1, 1) * 0.5f;
                cube.transform.position = tile.position;
                //foreach (int index in tile.vertices)
                //{
                //    GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Capsule);
                //    cube.GetComponent<CapsuleCollider>().enabled = false;
                //    cube.transform.localScale = new Vector3(1, 1, 1) * 0.5f;
                //    cube.transform.position = vertices[index];
                //}
                yield return null;
            }
        }
    }
    public void         AssertPreconditions()
    {
        Dictionary<int, bool> dict = new Dictionary<int, bool>();

        foreach(Tile tile in tiles)
        {
            foreach(int index in tile.vertices)
            {
                if (dict.ContainsKey(index))
                    Debug.Assert(false, "fucked up bad at inserting tile vertices");
                else
                    dict.Add(index, true);
            }
        }
    }
}

public class GridManager : MonoBehaviour
{
    public Grid grid;

    public void InitGrid(int sizeX, int sizeZ)
    {
        grid = new Grid(sizeX, sizeZ);
    }

    //private void OnDrawGizmos()
    //{
    //    if (grid == null)
    //        return;

    //    Tile[,] tiles = grid.GetTiles();
    //    for (int x = 0; x < grid.sizeX; x++)
    //    {
    //        for (int z = 0; z < grid.sizeZ; z++)
    //        {
    //            Tile tile = tiles[x, z];
    //            Gizmos.DrawSphere(tile.position, 0.2f);
    //        }
    //    }
    //}

    // Temp function for debugging
    public void StartCustonCoroutine(IEnumerator coroutine)
    {
        StartCoroutine(coroutine);
    }

    // Singleton Properties
    public static GridManager Instance { get; private set; }
    private void Awake()
    {
        // If there is an instance, and it's not me, delete myself.

        if (Instance != null && Instance != this)
        {
            Destroy(this);
        }
        else
        {
            Instance = this;
        }
    }
}
