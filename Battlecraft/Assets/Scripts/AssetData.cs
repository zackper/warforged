using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class AssetData : MonoBehaviour
{
    public new string   name;
    public string       category;
    public string       fileType;
    public string       mainTag;
    public string[]     tags;
    public bool         isFavorite;

    public GameObject   self;
    public Texture2D    texture;

    public bool         isStructure;
    public bool         isLockedOnTerrain = true;

    private void Awake()
    {
        if(fileType == "prefab")
        {
            GameObjectManager.Instance.Add(self, mainTag);
        }
    }
    private void OnDestroy()
    {
        if (fileType == "prefab")
        {
            GameObjectManager.Instance.Remove(self, mainTag);
        }
    }
}
