using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

[System.Serializable]
public class AssetLoader
{
    // For json parsiing
    public string[] fileNames;

    public List<AssetData> assetsData = new List<AssetData>();
    private static Dictionary<string, List<AssetData>> cachedAssets = new Dictionary<string, List<AssetData>>();

    public AssetLoader(string bundleName)
    {
        if (cachedAssets.ContainsKey(bundleName))
        {
            assetsData = cachedAssets[bundleName];
            return;
        }

        TextAsset json = Resources.Load<TextAsset>("BundleLists/" + bundleName);
        JsonUtility.FromJsonOverwrite(json.text, this);

        // All bundles will be on the same location
        AssetBundle bundle = AssetBundle.LoadFromFile(Path.Combine(Application.streamingAssetsPath, bundleName));
        if (bundle == null)
        {
            Debug.Log("Failed to load asset bundle named \'" + bundleName + "\'.");
            return;
        }
        foreach (string fileName in fileNames)
        {
            GameObject prefab = bundle.LoadAsset<GameObject>(fileName);
            assetsData.Add(prefab.GetComponent<AssetData>());
        }

        // Add to cached bundles
        cachedAssets.Add(bundleName, assetsData);
    }
    public AssetData this[string name]
    {
        get
        {
            foreach (KeyValuePair<string, List<AssetData>> pair in cachedAssets)
                foreach (AssetData assetData in pair.Value)
                    if (assetData.name == name)
                        return assetData;
            return null;
        }
    }


    public static void UnloadAll()
    {
        AssetBundle.UnloadAllAssetBundles(false);
    }
}
