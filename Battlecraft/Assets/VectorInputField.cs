using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace SystemToolUI
{
    public class VectorInputField : MonoBehaviour
    {
        [SerializeField]
        public NumberInputField x, y, z;

        public Vector3 value
        {
            get
            {
                return new Vector3(x.value, y.value, z.value);
            }
            set
            {
                x.value = value.x;
                y.value = value.y;
                z.value = value.z;
            }
        }

        public float increamentAmount
        {
            get
            {
                return x.increamentAmount;
            }
            set
            {
                ApplyToAll((NumberInputField field) => {
                    field.increamentAmount = value;
                });
            }
        }
        public string postfix
        {
            get
            {
                return x.postfix;
            }
            set
            {
                ApplyToAll((NumberInputField field) => {
                    field.postfix = value;
                });
            }
        }
        public string format
        {
            get
            {
                return x.format;
            }
            set
            {
                   ApplyToAll((NumberInputField field) => {
                       field.format = value;
                   });
            }
        }

        public UnityEvent onValueChanged = new UnityEvent();

        private void Start()
        {
            x.onValueChanged.AddListener(delegate { onValueChanged.Invoke(); });
            y.onValueChanged.AddListener(delegate { onValueChanged.Invoke(); });
            z.onValueChanged.AddListener(delegate { onValueChanged.Invoke(); });
        }

        // Util function to apply to all number fields
        private void ApplyToAll(UnityAction<NumberInputField> callback)
        {
            callback(x);
            callback(y);
            callback(z);
        }
    }
}