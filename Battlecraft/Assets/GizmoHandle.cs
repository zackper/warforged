using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GizmoHandle : MonoBehaviour
{
    public enum TransformMode {
        TRANSLATE, ROTATE, SCALE
    }
    private static TransformMode mode = TransformMode.TRANSLATE;

    public UnityEvent onTransformChange = new UnityEvent();
    public UnityEvent onSelectionChange = new UnityEvent();
    public AssetData selected;

    // Delegate functions and their instances. Translate/Rotate/Scale may work differently
    private delegate void   TransformStart(RaycastHit hit);
    private TransformStart  transformStart;
    private delegate void   TransformUpdate(Vector3 axis);
    private TransformUpdate transformUpdate;
    private delegate void   TransformEnd();
    private TransformEnd    transformEnd; 


    [SerializeField] private GameObject arrowGroup;
    [SerializeField] private GameObject planeGroup;
    [SerializeField] private GameObject torusGroup;
    
    [SerializeField] private GameObject xArrow;
    [SerializeField] private GameObject yArrow;
    [SerializeField] private GameObject zArrow;

    [SerializeField] private GameObject xzPlane;
    [SerializeField] private GameObject zyPlane;
    [SerializeField] private GameObject xyPlane;

    [SerializeField] private GameObject xTorus;
    [SerializeField] private GameObject yTorus;
    [SerializeField] private GameObject zTorus;

    // Variables used for transform handling
    private Vector3 initialOffset;
    private Vector3 initialTransform;
    private Vector3 transformAxis;

    private void Start()
    {
        ChangeMode(mode);
    }
    public void ChangeMode(TransformMode newMode)
    {
        mode = newMode;
        switch (mode)
        {
            case TransformMode.TRANSLATE:
                transformStart = Translate_Start;
                transformUpdate = Translate_Update;
                transformEnd = Translate_End;
                arrowGroup.SetActive(true);
                planeGroup.SetActive(true);
                torusGroup.SetActive(false);
                break;
            case TransformMode.ROTATE:
                transformStart = Rotate_Start;
                transformUpdate = Rotate_Update;
                transformEnd = Rotate_End;
                arrowGroup.SetActive(false);
                planeGroup.SetActive(false);
                torusGroup.SetActive(true);
                break;
        }
    }

    private void Update()
    {
        if (selected == null)
            return;

        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            int mask = LayerMask.GetMask("Gizmos");
            if (Physics.Raycast(ray, out RaycastHit hitInfo, 250, mask))
            {
                transformStart(hitInfo);
            }

            //SetIgnoreRaycast(true);
        }
        else if (Input.GetMouseButton(0))
        {
            transformUpdate(transformAxis);
            onTransformChange.Invoke();
        }
        else if (Input.GetMouseButtonUp(0))
        {
            GameObjectManager.Instance.UpdateObject(selected.self);
            transformAxis = Vector3.zero;
            transformEnd();
            //SetIgnoreRaycast(false);
        }
        else if (Input.GetKeyDown(KeyCode.Q))
            ChangeMode(TransformMode.TRANSLATE);
        else if (Input.GetKeyDown(KeyCode.W))
            ChangeMode(TransformMode.ROTATE);

        // if on rotate mode, always make toruses look at the camera.
        if (mode == TransformMode.ROTATE)
            TorusesLookAtCamera();
    }

    public void Translate_Start(RaycastHit hitInfo)
    {
        if (hitInfo.transform.gameObject == xArrow)
            transformAxis = new Vector3(1, 0, 0);
        else if (hitInfo.transform.gameObject == yArrow)
            transformAxis = new Vector3(0, 1, 0);
        else if (hitInfo.transform.gameObject == zArrow)
            transformAxis = new Vector3(0, 0, 1);
        else if (hitInfo.transform.gameObject == xzPlane)
            transformAxis = new Vector3(1, 0, 1);
        else if (hitInfo.transform.gameObject == zyPlane)
            transformAxis = new Vector3(0, 1, 1);
        else if (hitInfo.transform.gameObject == xyPlane)
            transformAxis = new Vector3(1, 1, 0);

        SetPlaneGroupCollidersActive(false);
        if ((transformAxis.x == 1 || transformAxis.z == 1) && transformAxis.y == 0)
        {
            BoxCollider collider = xzPlane.GetComponent<BoxCollider>();
            collider.enabled = true;
            collider.size = new Vector3(1000f, 0, 1000f);
        }
        else if (transformAxis.y == 1 && transformAxis.z == 1)
        {
            BoxCollider collider = zyPlane.GetComponent<BoxCollider>();
            collider.enabled = true;
            collider.size = new Vector3(1000f, 0, 1000f);
        }
        else if (transformAxis.y == 1 && transformAxis.x == 1)
        {
            BoxCollider collider = xyPlane.GetComponent<BoxCollider>();
            collider.enabled = true;
            collider.size = new Vector3(1000f, 1000f, 0f);
        }
        else // Any y combination
        {
            BoxCollider collider1 = zyPlane.GetComponent<BoxCollider>();
            collider1.enabled = true;
            collider1.size = new Vector3(0f, 1000f, 1000f);
            BoxCollider collider2 = xyPlane.GetComponent<BoxCollider>();
            collider2.enabled = true;
            collider2.size = new Vector3(1000f, 1000f, 0f);
        }
        
        int mask = LayerMask.GetMask("Gizmos");
        MouseRaycast(out RaycastHit mouseInfo, mask);
        initialOffset = mouseInfo.point - transform.position;
    }
    public void Translate_Update(Vector3 axis)
    {
        // Get mouse raycasted position to world
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        LayerMask mask = LayerMask.GetMask("Gizmos");
        if(Physics.Raycast(ray, out RaycastHit hitInfo, 250f, mask))
        {
            transform.position = new Vector3(
                axis.x == 0 ? transform.position.x : hitInfo.point.x - initialOffset.x,
                axis.y == 0 ? transform.position.y : hitInfo.point.y - initialOffset.y,
                axis.z == 0 ? transform.position.z : hitInfo.point.z - initialOffset.z
            );
            selected.self.transform.position = transform.position;
        }
    }
    public void Translate_End()
    {
        SetPlaneGroupCollidersActive(true);
        for(int i = 0; i < planeGroup.transform.childCount; i++)
        {
            BoxCollider collider = planeGroup.transform.GetChild(i).GetComponent<BoxCollider>();
            collider.size = Vector3.one;
        }
    }

    public void Rotate_Start(RaycastHit hitInfo)
    {
        // Disable all and later enable the one hit
        GameObject[] toruses = { xTorus, yTorus, zTorus };
        foreach (GameObject torus in toruses)
        {
            torus.GetComponent<BoxCollider>().enabled = true;
            torus.SetActive(false);
        }

        // Leave enabled only the torus actually hit
        if (hitInfo.transform.gameObject == xTorus)
        {
            transformAxis = new Vector3(1, 0, 0);
            xTorus.SetActive(true);
        }
        else if (hitInfo.transform.gameObject == yTorus)
        {
            transformAxis = new Vector3(0, 1, 0);
            yTorus.SetActive(true);
        }
        else if (hitInfo.transform.gameObject == zTorus)
        {
            transformAxis = new Vector3(0, 0, 1);
            zTorus.SetActive(true);
        }

        initialOffset = hitInfo.point;
        initialTransform = hitInfo.transform.eulerAngles;

        // Enlarge box collider of torus hit by a bunch :)
        BoxCollider collider = hitInfo.transform.GetComponent<BoxCollider>();
        collider.size = new Vector3(0.02f, 50f, 50f);
    }
    public void Rotate_Update(Vector3 axis)
    {
        // Get mouse raycasted position to world
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        int mask = LayerMask.GetMask("Gizmos");
        if (Physics.Raycast(ray, out RaycastHit hitInfo, 100, mask))
        {
            Vector3 initial = initialOffset - hitInfo.transform.position;
            Vector3 current = hitInfo.point - hitInfo.transform.position;

            float dot = Vector3.Dot(initial, current);
            float radians = Mathf.Acos( dot / (Vector3.Magnitude(initial) * Vector3.Magnitude(current)) );
            float angle = radians * 180 / Mathf.PI;

            transform.eulerAngles = new Vector3(
                axis.x == 0 ? transform.eulerAngles.x : initialTransform.x + angle,
                axis.y == 0 ? transform.eulerAngles.y : initialTransform.y + angle,
                axis.z == 0 ? transform.eulerAngles.z : initialTransform.z + angle
            );

            if (angle > 80)
            {
                initialTransform = transform.eulerAngles;
                initialOffset = hitInfo.point;
            }
        }

        selected.self.transform.eulerAngles = transform.eulerAngles;
    }
    public void Rotate_End()
    {
        // Revert torus collider to normal
        GameObject[] toruses = { xTorus, yTorus, zTorus };
        foreach (GameObject torus in toruses)
        {
            torus.SetActive(true);
            torus.GetComponent<BoxCollider>().enabled = false;
        }
    }

    public void SetSelected(AssetData selected)
    {
        this.selected = selected;
        transform.position = selected.self.transform.position;
        //transform.eulerAngles = selected.self.transform.eulerAngles;
        onSelectionChange.Invoke();
    }

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // Util functions
    public bool MouseRaycast(out RaycastHit hitInfo, int mask = 0)
    {
        // Get mouse raycasted position to world
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        return Physics.Raycast(ray, out hitInfo, 100, mask);
    }
    private void SetIgnoreRaycast(bool active)
    {
        if (active)
        {
            xArrow.layer = LayerMask.NameToLayer("Ignore Raycast");
            yArrow.layer = LayerMask.NameToLayer("Ignore Raycast");
            zArrow.layer = LayerMask.NameToLayer("Ignore Raycast");
            selected.gameObject.layer = LayerMask.NameToLayer("Ignore Raycast");
        }
        else
        {
            xArrow.layer = LayerMask.NameToLayer("Gizmos");
            yArrow.layer = LayerMask.NameToLayer("Gizmos");
            zArrow.layer = LayerMask.NameToLayer("Gizmos");
            selected.gameObject.layer = LayerMask.NameToLayer("Default");
        }
    }
    private void TorusesLookAtCamera()
    {
        Vector3 previous;
        Vector3 current;

        previous = xTorus.transform.eulerAngles;
        xTorus.transform.LookAt(Camera.main.transform);
        current = xTorus.transform.eulerAngles;
        xTorus.transform.eulerAngles = new Vector3(current.x, previous.y, previous.z);

        previous = yTorus.transform.eulerAngles;
        yTorus.transform.LookAt(Camera.main.transform);
        current = yTorus.transform.eulerAngles;
        yTorus.transform.eulerAngles = new Vector3(previous.x, current.y, previous.z);

        previous = zTorus.transform.eulerAngles;
        zTorus.transform.LookAt(Camera.main.transform);
        current = zTorus.transform.eulerAngles;
        zTorus.transform.eulerAngles = new Vector3(previous.x, previous.y, current.z);
    }
    private void SetPlaneGroupCollidersActive(bool active)
    {
        for (int i = 0; i < planeGroup.transform.childCount; i++)
        {
            BoxCollider collider = planeGroup.transform.GetChild(i).GetComponent<BoxCollider>();
            collider.enabled = active;
        }
    }
}
