using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using TMPro;

namespace SystemToolUI
{
    public class ColorPicker : MonoBehaviour
    {
        public TMP_Text _title;
        public string title
        {
            get
            {
                return _title.text;
            }
            set
            {
                _title.text = value;
            }
        }

        [SerializeField]
        private Image _selectedColorImage;
        public Color color
        {
            get
            {
                return _selectedColorImage.color;
            }
            set
            {
                _selectedColorImage.color = value;
            }
        }

        public UnityEvent onColorChange = new UnityEvent();

        [SerializeField]
        private ColorWheel _colorWheel;


        private void Start()
        {
            _colorWheel.onColorChange.AddListener(delegate
            {
                _selectedColorImage.color = _colorWheel.color;
                onColorChange.Invoke();
            });
        }

        public void OnColorPickerDown()
        {
            _colorWheel.gameObject.SetActive(true);
        }
    }
}