using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using TMPro;

namespace SystemToolUI
{
    public class Checkbox : MonoBehaviour
    {
        public string title
        {
            get
            {
                return _titleLabel.text;
            }
            set
            {
                _titleLabel.text = value;
            }
        }
        public Button button;
        public bool isOn;
        public UnityEvent<bool> onToggleChange = new UnityEvent<bool>();

        [SerializeField]
        private TMP_Text _titleLabel;
        [SerializeField]
        private GameObject toggledOnPanel;

        private void Start()
        {
            button.onClick.AddListener(delegate
            {
                isOn = !isOn;
                toggledOnPanel.SetActive(isOn);
                onToggleChange.Invoke(isOn);
            });
        }
    }
}