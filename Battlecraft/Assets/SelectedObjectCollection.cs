using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.Events;

namespace SystemToolUI
{
    public class SelectedObjectCollection : MonoBehaviour
    {
        public TMP_Text titleLabel;
        public TMP_Text selectedName;
        public RawImage mainImage;
        public GridLayoutGroup secondaryImagesGrid;
        public UnityEvent onSelectedChange;

        public List<string> filteredTags = null;
        public List<AssetData> selectedCollection;

        public void OnButtonClick()
        {
            GameObject canvas = GameObject.Find("Canvas");
            GameObject assetCollectionObj = UIFactory.Instance.Create(Type.ASSET_COLLECTION, canvas.transform);
            AssetCollection assetCollection = assetCollectionObj.GetComponent<AssetCollection>();
            assetCollection.buttonClick.AddListener(delegate (AssetData data)
            {
                selectedCollection.Clear();
                selectedCollection.Add(data);
                mainImage.texture = data.texture;
                onSelectedChange.Invoke();
                assetCollection.Close();
            });
            assetCollection.Load("objects", filteredTags);
        }
    }
}
